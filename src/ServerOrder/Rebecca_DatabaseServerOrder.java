//package ServerOrder;
//
//import java.util.ArrayList;
//
//import control.clientDataReceiving.MessageForClient;
//import model.datafeildStuff.DataFeildList;
//import model.serverStuff.serverCommunication.ReturnOrder;
//import orders.ErrorLocation;
//import orders.databaseOrders.DataOrder;
//
//// TODO: Auto-generated Javadoc
///**
// * The Class Rebecca_DatabaseServerOrder, which represents an order for the server to do
// * something to the database this is supposed to be made when anything needs a
// * call to the database.
// */
//public class Rebecca_DatabaseServerOrder extends ServerOrder {
//
//	/** The Constant serialVersionUID. */
//	private static final long serialVersionUID = 4L;
//
//	/** The data order. */
//	protected DataOrder dataOrder;
//	protected ReturnOrder forClient;
//
//	/** The distribute order. */
//	
//	/**
//	 * Instantiates a new server command.
//	 *
//	 * @param serverOrder
//	 *            the server order
//	 */
//	public Rebecca_DatabaseServerOrder(DataOrder serverOrder,int id) {
//		this.dataOrder = serverOrder;
//		this.forClient=new ReturnOrder(new MessageForClient("",null), id);
//	}
//	public Rebecca_DatabaseServerOrder(DataOrder serverOrder) {
//		this.dataOrder=serverOrder;
//		this.forClient=new ReturnOrder(new MessageForClient("",null),-1);
//	}
//	public void setDistributeId(int id) {
//		this.forClient.setDistributeID(id);
//	}
//	/**
//	 * Gets the database order of the server command, ie what the command will do to
//	 * the database.
//	 *
//	 * @return the database order
//	 */
//	public DataOrder getDatabaseOrder() {
//		return dataOrder;
//	}
//
//	/**
//	 * Gets the distribute order of the serverCommand, ie what will happen to the
//	 * results of the order after the results are sent to the client side.
//	 *
//	 * @return the distribute order
//	 */
//	
//
//	/**
//	 * Executes an order meant for the server, which is the psudo "main" for the
//	 * server action. in this particular case, involves calling the database to do
//	 * some database function, finding the result of that and sending it to the
//	 * packager
//	 */
//	@Override
//	public void execute() {
//
//		// when sucessful, the message is that the order completed successfully
//		String message = dataOrder.getName() + " completed sucessfully";
//
//		// when unsucessful, list has one element, being the original datafeilds from
//		// the dataOrder
//		ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
//		list.add(dataOrder.getList());
//		try {
//			list = super.getBox().getRouter().executeOrder(dataOrder);
//		} catch (IllegalArgumentException e) {
//			// when unsuccessful, the message is that it was not successful
//			message = e.getMessage();
//		}
//
//		forClient.getMessage().setList(list);
//		forClient.getMessage().setMessage(message);
//
//		super.getBox().getSender().send(forClient);
//	}
//
//	/**
//	 * Sets the error of the server order to the inputed error message.
//	 * 
//	 * @param errorMessage
//	 *            the new error message which the databaseServerOrder will have
//	 */
//	public void addError(String errorMessage, ErrorLocation location) {
//		super.addError(errorMessage, location);
//	}
//}
