package ServerOrder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import model.labels.SubmissionLabel;
import orders.databaseOrders.InsertOrder;

public class FileFromGUIToServerOrder extends DatabaseServerOrder {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	byte[] content;
	String myTextPath;
	Path myPath;
	// String STORAGE_PATH;
	// String FILE_NAME;
	// String FILE_EXTENSION;

	public FileFromGUIToServerOrder(InsertOrder serverOrder) {
		super(serverOrder); 

		myTextPath = serverOrder.getList().get(SubmissionLabel.PATH).getData();
		myPath = Paths.get(myTextPath);
		File selectedFile = new File(myTextPath); // Add your file myTextPath here
		long length = selectedFile.length();
		content = new byte[(int) length]; // Converting Long to Int
		try {
			FileInputStream fis = new FileInputStream(selectedFile);
			BufferedInputStream bos = new BufferedInputStream(fis);
			bos.read(content, 0, (int) length);

			bos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		myTextPath = null;
		myPath = null;
	}

	public static String escapePath(String path) {
		return path.replace("\\", "\\\\");
	}

	@Override
	public void execute() {
		myTextPath = super.getDatabaseOrder().getList().get(SubmissionLabel.PATH).getData();
		myPath = Paths.get(myTextPath);
		// File newFile = new File(STORAGE_PATH + FILE_NAME + FILE_EXTENSION);
		File newFile = new File(System.getProperty("user.home") + "\\Downloads\\" + myPath.getFileName());
		// File newFile = new File(myTextPath);
		try {
			if (!newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(content);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String myEscapedPath = escapePath(System.getProperty("user.home") + "\\Downloads\\" + myPath.getFileName());
		super.getDatabaseOrder().getList().get(SubmissionLabel.PATH).setData(myEscapedPath);
		super.execute();
	}

}
