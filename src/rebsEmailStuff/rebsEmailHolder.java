package rebsEmailStuff;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class rebsEmailHolder {

	Properties properties = new Properties();
	Session session;
	String MY_ADDRESS = "princess.columbidae@gmail.com";
	String MY_PASSWORD = "ensf_409";
	ArrayList<String> localListOfRecipientsEmails;
	InternetAddress[] recipientEmailsFromArg;
	String myMessageSubject = "Your Message Subject";
	String myMessageContent = "Your Message Content";
	// InternetAddress[] MY_RECIPIENT_ADDRESS = {new
	// InternetAddress("princess.columbidae@gmail.com")};

	public rebsEmailHolder(ArrayList<String> input, String inputMessageSubject, String inputMessageContent) {
		localListOfRecipientsEmails = input;
		System.out.println(input);
		
		myMessageContent = inputMessageContent;
		myMessageSubject = inputMessageSubject;
		rebsAddRecipientstoEmail();

		initializeProperties();

		session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(MY_ADDRESS, MY_PASSWORD);
			}
		});

		assemblingAndSendingEmail();
	}

	public void assemblingAndSendingEmail() {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(MY_ADDRESS));
			// InternetAddress[] MY_RECIPIENT_ADDRESS = { new
			// InternetAddress("princess.columbidae@gmail.com") };
			message.addRecipients(Message.RecipientType.TO, recipientEmailsFromArg);
			message.setSubject(myMessageSubject);
			message.setText(myMessageContent);
			Transport.send(message); // Send the Email Message
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		System.out.println("at the end of assemblingAndSendingEmail");
	}

	public void rebsAddRecipientstoEmail() {
		recipientEmailsFromArg = new InternetAddress[localListOfRecipientsEmails.size()];
		try {
			for (int i = 0; i < localListOfRecipientsEmails.size(); i++) {
				recipientEmailsFromArg[i] = new InternetAddress(localListOfRecipientsEmails.get(i));
			}
		} catch (AddressException e) {
			e.printStackTrace();
		}
		System.out.println("At the end of rebsAddRecipients");
	}

	public void initializeProperties() {
		properties.put("mail.smtp.starttls.enable", "true"); // Using TLS
		properties.put("mail.smtp.auth", "true"); // Authenticate
		properties.put("mail.smtp.host", "smtp.gmail.com"); // Using Gmail Account
		properties.put("mail.smtp.port", "587"); // TLS uses port 587
	}

}
