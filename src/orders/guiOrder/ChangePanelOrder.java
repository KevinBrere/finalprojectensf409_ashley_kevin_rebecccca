package orders.guiOrder;

import javax.swing.JPanel;

import view.professor.Professor;
import view.professor.panelNames;
import view.student.Student;



/**
 * The Class ChangePanelOrder.
 * Changes which panel is visible, and runs setup to display the specifics for the panel.
 * @author Ashley
 */
public class ChangePanelOrder extends GUIOrder implements panelNames{
	
	/** The panel name. */
	String panelName;
	
	/** The setup data. */
	String []setupData;
	
	/**
	 * Instantiates a new change panel order.
	 *
	 * @param a the a
	 * @param b the b
	 */
	public ChangePanelOrder(String a, String[]b) {
		panelName = a;
		setupData = b;
	}
	
//	public ChangePanelOrder(String a) {
//		panelName = a;
////		setupData = b;
//	}

	/* (non-Javadoc)
 * @see orders.SystemOrder#execute()
 */
//	@Override
	public void execute() {
		// TODO Auto-generated method stub
		
		System.out.println(panelName + " in order");
		JPanel panel = view.getCurrentGUI();
		if(panel instanceof Professor) {
			Professor x = (Professor)panel;
			x.changePanelDisplayed(panelName, setupData);
			
		}
		else if(panel instanceof Student) {
			 Student x = (Student)panel;
			 x.changePanelDisplayed(panelName, setupData);
			
		}

	}
	
	/**
	 * Sets the argument.
	 *
	 * @param s the new argument
	 */
	public void setArgument(String s) {
		panelName = s;
	}

}
