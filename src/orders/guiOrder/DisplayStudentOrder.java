package orders.guiOrder;


/**
 * The Class DisplayStudentOrder.
 * Displays the Student GUI if a student logs in
 * @author Ashley
 */
public class DisplayStudentOrder extends GUIOrder{
	
	/** The student ID. */
	private String studentID;
	
	/**
	 * Instantiates a new display student order.
	 *
	 * @param id the id
	 */
	public DisplayStudentOrder(String id) {
		studentID = id;
	}
	
	/* (non-Javadoc)
	 * @see orders.SystemOrder#execute()
	 */
	@Override
	public void execute() {
		view.displayStudent(studentID);
//		System.out.println("ORDER");
	}

}
