package orders.databaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import model.labels.DataLabel;

/**
 * The Class SearchOrder, which represents an order for the database to search
 * for several data peices
 */
public class SearchOrder implements DataOrder {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant name. */
	private static final String name = "Search";

	/** The list which will be inserted into the database */
	private DataFeildList list;

	/** The statement. which it will execute to insert into the database */
	private PreparedStatement statement;
	public Connection jdbc_connection;
	/**returns the name of the order, ie search*/
	@Override
	public String getName() {
		return name;
	}

	/**
	 * executes the order to search  the database
	 * @return the result of the search
	 */
	@Override
	public ArrayList<DataFeildList> execute() {

		if (list == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the list to the order");
		}

		if (this.jdbc_connection == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the preparedstatement to the order");
		}
		
		return search();
	}
	/** does the searching process*/
	private ArrayList<DataFeildList> search() {
		
		String sql = "SELECT * FROM " + list.get(0).getLabel().getTableName()+ " WHERE ";
		
		String nextDataEntry="'"+list.get(0).getData()+"'";
		
		sql += list.get(0).getLabel().getColumnName() + "= " + nextDataEntry;
		
		for (int i = 1; i < list.size(); i++) {
			nextDataEntry="'"+list.get(i).getData()+"'";
			sql += " AND " + list.get(i).getLabel().getColumnName() + " = " + nextDataEntry;
		}
		System.out.println(sql);
		DataLabel topLabel = list.get(0).getLabel();

		ArrayList<DataFeildList> tableMatch = new ArrayList<DataFeildList>();

		ResultSet myClient;
		try {

			statement = jdbc_connection.prepareStatement(sql);

			myClient = statement.executeQuery();

			tableMatch = new ArrayList<DataFeildList>();
			
			while (myClient.next()) {
			
				System.out.println("added");
				tableMatch.add(getResult(myClient, topLabel));
			}
			System.out.println("before loop");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tableMatch;

	}
	/** translates a result set into a datafeildList */
	private DataFeildList getResult(ResultSet myClient, DataLabel label) throws IllegalArgumentException, SQLException {
		DataFeildList list = new DataFeildList();
		for (int i = 1; i < label.getAllLabels().size()+1; i++) {
			list.add(new model.datafeildStuff.DataFeild(label.getLabelBasedOnColumnNumber(i), myClient.getString(i)));
		}
		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * orders.databaseOrders.DataOrder#setData(model.datafeildStuff.DataFeildList)
	 */
	@Override
	public void setData(DataFeildList list) {
		this.list = list;

	}

	@Override
	public DataFeildList getList() {
		return list;
	}
	
	@Override
	public void setConnection(Connection connection) {
		this.jdbc_connection = connection;
	}

}
