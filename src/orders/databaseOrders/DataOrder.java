package orders.databaseOrders;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;

// TODO: Auto-generated Javadoc
/**
 * The Interface DataOrder, which represents any order to modify the database.  ie searching, inserting
 */
public interface DataOrder extends Serializable {
	
	/**
	 * Gets the name of the order, ie search, modify.
	 *
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * Sets the data being searched/modified
	 *
	 * @param list the new data
	 */
	public abstract void setData(DataFeildList list);

	/**
	 * Executes the dataOrder. Which does what the dataOrder wants to do.  like saving a peice of data
	 *
	 * @return several data field lists which represents data the client needs after the dataOrder has executed. 
	 * ie after a search, it will return all of the people being searched for
	 */
	public abstract ArrayList<DataFeildList> execute();

	/**
	 * Sets the connection to the connection which links it to the database.  
	 *
	 * @param statement the new statement
	 */
	public abstract void setConnection(Connection connection);

	/**
	 * Gets the list of the pieces of data the DataOrder uses.
	 *
	 * @return the list
	 */
	public abstract DataFeildList getList();
}
