package orders.databaseOrders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.DataLabel;

/**
 * The Class InsertOrder, which represents an order for the database to insert a
 * peice of data
 */
public class InsertOrder implements DataOrder {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant name. */
	private static final String name = "insertion";

	/** The list which will be inserted into the database */
	private DataFeildList list;

	/** The statement. which it will execute to insert into the database */
	private PreparedStatement statement;
	public Connection jdbc_connection;

	/*
	 * (non-Javadoc)
	 * 
	 * @see orders.databaseOrders.DataOrder#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * executes the order to insert into the database
	 * 
	 * @return the person being inserted
	 */
	@Override
	public ArrayList<DataFeildList> execute() {
		if (list == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the list to the order");
		}
		if (this.jdbc_connection == null) {
			throw new IllegalArgumentException("error, the programer forgot to add the preparedstatement to the order");
		}
		insert();
		ArrayList<DataFeildList> array = new ArrayList<DataFeildList>();
		list.add(new DataFeild(list.get(0).getLabel().getLabelBasedOnColumnNumber(1),getLastID()));
		array.add(list);
		return array;

	}
	/** inserts a peice of data into the database*/
	private void insert() {

		DataLabel top = list.get(0).getLabel();
		System.out.println(top.getTableName());
		String sql = "INSERT INTO " + top.getTableName() + " (";
		for (int i = 0; i < list.size() - 1; i++) {
			sql += list.get(i).getLabel().getColumnName()+",";
		}
		sql += list.get(list.size()-1).getLabel().getColumnName()+")";
		sql+="VALUES(";
		for (int i = 0; i < list.size()-1; i++) {
			sql+= "'"+list.get(i).getData()+"', ";
		}
		sql+="'"+list.get(list.size()-1).getData()+"')";
		
		try {
			
			statement = jdbc_connection.prepareStatement(sql);

			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(
					"error, we were unable to insert your data into the table.  make sure that it does not already exist");
		}

		return;
	}
	/** returns the id of the person just inserted. */
	private String getLastID() {
		int lastId=-1;
		try {
			Statement state2=jdbc_connection.createStatement();
			String sql="SELECT LAST_INSERT_ID()";
			ResultSet resSet=state2.executeQuery(sql);
			while(resSet.next()) {
			lastId=resSet.getInt(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("error with assigning id, please contact technical support for more information");
		}
		
		return Integer.toString(lastId);
		
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * orders.databaseOrders.DataOrder#setData(model.datafeildStuff.DataFeildList)
	 */
	@Override
	public void setData(DataFeildList list) {
		this.list = list;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see orders.databaseOrders.DataOrder#setStatement(java.sql.PreparedStatement)
	 */
	@Override
	public void setConnection(Connection connection) {
		this.jdbc_connection = connection;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see orders.databaseOrders.DataOrder#getList()
	 */
	@Override
	public DataFeildList getList() {
		return list;
	}

}
