package orders.messengerOrders;

import messenger.MessageRoomUserInfo;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class LeaveChatRoomOrder, which is an order for the messenging service on the server to 
 * remove a user from the chat room.  
 */
public class LeaveChatRoomOrder extends MessengerOrder {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4606485117200264670L;

	/**
	 * Instantiates a new leave chat room order.
	 *
	 * @param messege the data for the student who will leave the chat room.  
	 */
	public LeaveChatRoomOrder(DataFeildList messege) {
		super(messege);
	}

	/**
	 * executes the order, ie makes the user leave the chat room.
	 */
	@Override
	public void execute() {
		try {
			MessageRoomUserInfo info = new MessageRoomUserInfo(super.getBox().getSender(), super.getDistributeID(),
					super.getMessegeContentList().get(MessageLabel.FROM).getData());

			String classId = super.getMessegeContentList().get(MessageLabel.CLASSID).getData();

			super.getBox().getMessager().exitRoom(info, classId);
		} catch (IllegalArgumentException e) {
			super.handleError(e);
		}
	}

}
