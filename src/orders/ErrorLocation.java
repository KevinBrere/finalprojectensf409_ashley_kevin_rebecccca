package orders;
/**
 * class which represents the location of an error.  ie if an error occurs on the order, 
 * where did it happen?  part of an error log. 
 * @author brere
 *
 */
 public enum ErrorLocation {
	GUI,SERVER;
}
