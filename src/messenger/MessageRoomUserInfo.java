package messenger;

import model.serverStuff.serverCommunication.SocketSender;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageRoomUserInfo, which represents the information the message chat room needs 
 * to send/recieve data
 */
public class MessageRoomUserInfo {
	
	/** The sender, for sending messages through. */
	private SocketSender sender;
	
	/** The distribute id. */
	private int distributeId;
	
	/** The name. */
	private String name;

	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	protected SocketSender getSender() {
		return sender;
	}

	/**
	 * Gets the distribute id
	 *
	 * @return the distribute id
	 */
	protected int getDistributeId() {
		return distributeId;
	}

	/**
	 * Instantiates a new message room user info
	 * @param sender the sender
	 * @param distributeId the distribute id
	 * @param name the name
	 */
	public MessageRoomUserInfo(SocketSender sender, int distributeId, String name) {
		this.sender = sender;
		this.distributeId = distributeId;
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Equals.
	 *
	 * @param info the info
	 * @return true, if successful
	 */
	protected boolean equals(MessageRoomUserInfo info) {
		return this.name.equals(info.name);
	}

}
