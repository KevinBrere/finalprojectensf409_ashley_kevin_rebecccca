package messenger;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import model.serverStuff.serverCommunication.ReturnOrder;


// TODO: Auto-generated Javadoc
/**
 * The Class MessageRoom, which is a messing chatroom for one class
 */
public class MessageRoom {
	
	/** The class ID of the class. */
	String classID;
	
	/** The users. */
	ArrayList<MessageRoomUserInfo> users;

	/**
	 * Instantiates a new message room.
	 *
	 * @param id the id
	 */
	public MessageRoom(String id) {
		classID = id;
		users = new ArrayList<MessageRoomUserInfo>();
	}

	/**
	 * Joins a user to the room
	 *
	 * @param userInfo the user info
	 */
	public void joinRoom(MessageRoomUserInfo userInfo) {
		users.add(userInfo);
		System.out.println("added successfully, " + userInfo.getName() + " into: " + classID);
	}

	/**
	 * Exits a user from the room.
	 *
	 * @param userInfo the user info
	 */
	protected void exitRoom(MessageRoomUserInfo userInfo) {
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).equals(userInfo)) {
				users.remove(i);
				System.out.println("removed successfully, " + userInfo.getName() + "from: " + classID);
				return;
			}
		}
		throw new IllegalArgumentException("error, the student is not in the class it wants to exit from");
	}

	/**
	 * Sends a message to all users in the room
	 *
	 * @param list the list
	 */
	public void sendMessage(DataFeildList list) {
		if (list.isEmpty() || !(list.get(0).getLabel() instanceof MessageLabel)) {
			throw new IllegalArgumentException("error, you must send a message of type label");
		}
		for (int i = 0; i < users.size(); i++) {
			sendMessageToOneUser(list, users.get(i));
		}
	}

	/**
	 * Send message to one user.
	 *
	 * @param list the list
	 * @param sentTo the sent to
	 */
	private void sendMessageToOneUser(DataFeildList list, MessageRoomUserInfo sentTo) {
		DataFeild to = new DataFeild(MessageLabel.TO, sentTo.getName());
		list.add(to);
		MessageForClient client = constructClientMessage(list, sentTo);
		ReturnOrder sentToClient = new ReturnOrder(client, sentTo.getDistributeId());
		sentToClient.setDistributeDelete(false);
		sentTo.getSender().send(sentToClient);
		list.delete(MessageLabel.TO);

	}

	/**
	 * Construct client message.
	 *
	 * @param list the list
	 * @param sentTo the sent to
	 * @return the message for client
	 */
	private MessageForClient constructClientMessage(DataFeildList list, MessageRoomUserInfo sentTo) {
		ArrayList<DataFeildList> messageArray = new ArrayList<DataFeildList>();
		messageArray.add(list);
		MessageForClient client = new MessageForClient("message from " + list.get(MessageLabel.FROM), messageArray);

		return client;
	}

	/**
	 * Gets the class ID.
	 *
	 * @return the class ID
	 */
	protected String getClassID() {
		return classID;
	}

}
