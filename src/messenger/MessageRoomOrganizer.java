package messenger;

import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class MessageRoomOrganizer, which organizes all of the messsage rooms
 */
public class MessageRoomOrganizer {
	
	/** The message rooms. */
	private ArrayList<MessageRoom> rooms;

	/**
	 * Instantiates a new message room organizer.
	 */
	public MessageRoomOrganizer() {
		rooms = new ArrayList<MessageRoom>();
	}

	/**
	 * Join a user to a room.
	 *
	 * @param info the info
	 * @param classId the class id
	 */
	public void joinRoom(MessageRoomUserInfo info, String classId) {
		MessageRoom findroom = getRoomBasedOnClassId(classId);
		if (findroom == null) {
			findroom = new MessageRoom(classId);
			rooms.add(findroom);
		}
		findroom.joinRoom(info);
	}

	/**
	 * Exit a user from a room.
	 *
	 * @param info the info
	 * @param classID the class ID
	 */
	public void exitRoom(MessageRoomUserInfo info, String classID) {
		MessageRoom room = getRoomBasedOnClassId(classID);
		room.exitRoom(info);
	}

	/**
	 * Sends a message to a room .
	 *
	 * @param list the list
	 */
	public void sendMessage(DataFeildList list) {
		MessageRoom room = getRoomBasedOnClassId(list.get(MessageLabel.CLASSID).getData());
		if (room == null) {
			throw new IllegalArgumentException("error, we could not find your class, please try again later");
		}
		room.sendMessage(list);
	}

	/**
	 * Gets the room based on class id.
	 *
	 * @param classId the class id
	 * @return the room based on class id
	 */
	private MessageRoom getRoomBasedOnClassId(String classId) {
		for (int i = 0; i < rooms.size(); i++) {
			if (rooms.get(i).getClassID().equals(classId)) {
				return rooms.get(i);
			}
		}
		return null;
	}

}
