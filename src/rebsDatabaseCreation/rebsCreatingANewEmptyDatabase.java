package rebsDatabaseCreation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class rebsCreatingANewEmptyDatabase {

	/** The jdbc connection. */
	public Connection jdbc_connection;

	/** The statement. */
	public PreparedStatement statement;

	public String databaseName = "FinalProjectDatabase";
	private static final String connectionInfo = "jdbc:mysql://localhost:3306/", login = "FinalProjectDataTable",
			password = "ensf";

	public rebsCreatingANewEmptyDatabase() {
		// TODO Auto-generated constructor stub
	}

	// Use the jdbc connection to create a new database in MySQL.
	// (e.g. if you are connected to "jdbc:mysql://localhost:3306", the database
	// will be created at "jdbc:mysql://localhost:3306/InventoryDB")
	/**
	 * Creates the DB.
	 */

	public void createDB() {
		try {
			jdbc_connection = DriverManager.getConnection(connectionInfo, login, password);
			statement = jdbc_connection.prepareStatement("CREATE DATABASE " + databaseName);
			statement.executeUpdate();
			System.out.println("Created Database " + databaseName);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		rebsCreatingANewEmptyDatabase hello = new rebsCreatingANewEmptyDatabase();
		hello.createDB();
	}
}
