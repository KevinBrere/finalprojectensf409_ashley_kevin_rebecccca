package rebsDatabaseCreation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.*;
public class rebsDatabaseMaker  implements DataOrder{
	private static final long serialVersionUID = 2760648814266549043L;

	/** The jdbc connection. */
	private Connection jdbc_connection;

	/** The statement. */
	private PreparedStatement statement;
	
	private DataFeildList list;
//	/** The table name. */
//	public String databaseName = "ClientDB", tableName = "ClientTable";
//
//	// Students should configure these variables for their own MySQL environment
//	// If you have not created your first database in mySQL yet, you can leave the
//	// "[DATABASE NAME]" blank to get a connection and create one with the
//	/** The password. */
//	// createDB() method.
//	public String connectionInfo = "jdbc:mysql://localhost:3306/" + databaseName, login = "testingnewuser",
//			password = "password";

	public rebsDatabaseMaker() {
		list=new DataFeildList();
	}

	/**
	 * Create all data tables inside of the database to hold tools.
	 */
	private void createAllTables() {
		createUserTable();
		createCourseTable();
		createStudentEnrollmentTable();
		createAssignmentTable();
		createSubmissionTable();
		createGradeTable();
	}

	private void createUserTable() {
		// User Table
		// id: int (8)
		// password: varchar (20)
		// email: varchar (50)
		// firstname: varchar (30)
		// lastname: varchar (30)
		// type: char (1)
		String sql = "CREATE TABLE " + "UserTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, " 
				+ "PASSWORD VARCHAR(20) NOT NULL, "
				+ "EMAIL VARCHAR(50) NOT NULL, " 
				+ "FIRSTNAME VARCHAR(30) NOT NULL, "
				+ "LASTNAME VARCHAR(30) NOT NULL, " 
				+ "TYPE VARCHAR(1) NOT NULL, " 
				+ "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "UserTable");
		} catch (SQLException e) {
		}
	}

	private void createCourseTable() {
		// Course Table
		// id: int (8)
		// prof_id: int (8)
		// name: varchar (50)
		// active: bit (1)
		String sql = "CREATE TABLE " + "CourseTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT," 
				+ "PROF_ID VARCHAR(8) NOT NULL, "
				+ "NAME VARCHAR(50) NOT NULL, " 
				+ "ACTIVE VARCHAR(1) NOT NULL, " 
				+ "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "CourseTable");
		} catch (SQLException e) {
		}
	}

	private void createStudentEnrollmentTable() {
		// StudentEnrollment Table
		// id: int (8)
		// student_id: int (8)
		// course_id: int (8)
		String sql = "CREATE TABLE " + "StudentEnrollmentTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, "
				+ "STUDENT_ID VARCHAR(8) NOT NULL, " 
				+ "COURSE_ID VARCHAR(8) NOT NULL, " 
				+ "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "StudentEnrollmentTable");
		} catch (SQLException e) {
		}

	}

	private void createAssignmentTable() {
		// Assignment Table
		// id: int (8)
		// course_id: int (8)
		// title: varchar (50)
		// path: varchar (100)
		// active: bit (1)
		// due_date: char (16)
		//+ "EXTENSION VARCHAR(5) NOT NULL, "
		String sql = "CREATE TABLE " + "AssignmentTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, " 
				+ "COURSE_ID VARCHAR(8) NOT NULL, "
				+ "TITLE VARCHAR(50) NOT NULL, " 
				+ "PATH VARCHAR(100) NOT NULL, " 
				+ "ACTIVE VARCHAR(1) NOT NULL, "
				+ "DUE_DATE VARCHAR(16) NOT NULL, "
				+ "EXTENSION VARCHAR(5) NOT NULL, "
				+ "PRIMARY KEY ( id ))";
		
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "AssignmentTable");
		} catch (SQLException e) {
		}

	}

	private void createSubmissionTable() {
		// Submission Table
		// id: int (8)
		// assign_id: int (8)
		// student_id: int (8)
		// path: varchar (100)
		// title: varchar (50)
		// *submission_grade: int (3)
		// comments: varchar (140)
		// timestamp: char (16)
		// + "EXTENSION VARCHAR(5) NOT NULL, "
		String sql = "CREATE TABLE " + "SubmissionTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, " 
				+ "ASSIGNMENT_ID VARCHAR(8) NOT NULL, "
				+ "STUDENT_ID VARCHAR(8) NOT NULL, " 
				+ "PATH VARCHAR(100) NOT NULL, "
				+ "TITLE VARCHAR(50) NOT NULL, "
				+ "SUBMISSION_GRADE VARCHAR(3) NOT NULL, " 
				+ "COMMENTS VARCHAR(140) NOT NULL, "
				+ "TIMESTAMP VARCHAR(25) NOT NULL, " 
				+ "EXTENSION VARCHAR(5) NOT NULL, "
				+ "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "SubmissionTable");
		} catch (SQLException e) {
		}
	}

	private void createGradeTable() {
		// Grade Table
		// id: int (8)
		// assign_id: int (8)
		// student_id: int (8)
		// course_id: int (8)
		// *assignment_grade: int (3)

		String sql = "CREATE TABLE " + "GradeTable" + "(" 
				+ "ID INT(8) NOT NULL AUTO_INCREMENT, " 
				+ "ASSIGNMENT_ID VARCHAR(8) NOT NULL, "
				+ "STUDENT_ID VARCHAR(8) NOT NULL, " 
				+ "COURSE_ID VARCHAR(8) NOT NULL, " 
				+ "ASSIGNMENT_GRADE VARCHAR(3) NOT NULL, "
				+ "PRIMARY KEY ( id ))";
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Created Table " + "GradeTable");
		} catch (SQLException e) {
		}
	}

	@Override
	public String getName() {

		return "made database";
	}

	@Override
	public void setData(DataFeildList list) {
		this.list = list;
	}

	@Override
	public ArrayList<DataFeildList> execute() {
		createAllTables();
		ArrayList<DataFeildList> datamembers = new ArrayList<DataFeildList>();
		datamembers.add(list);
		return datamembers;
	}

	@Override
	public void setConnection(Connection connection) {
		this.jdbc_connection = connection;
	}

	@Override
	public DataFeildList getList() {
		return list;
	}

}
