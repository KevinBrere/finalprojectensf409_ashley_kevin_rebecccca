package rebsDatabaseCreation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//import data.dataFeild.DataFeild;
//import data.dataFeild.DataFeildList;
//import data.labels.CourseLabel;
//import data.labels.UserLabel;

public class rebsDatabaseManager {

	/** The jdbc connection. */
	public Connection jdbc_connection;

	/** The statement. */
	public PreparedStatement statement;

	public rebsDatabaseManager() {
		// TODO Auto-generated constructor stub
	}

	public void removeTable(String tableName) {
		String sql = "DROP TABLE " + tableName;
		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.executeUpdate();
			System.out.println("Removed Table " + tableName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void delete(String tableName, int id) {
		String sql = "DELETE FROM " + tableName + " WHERE ID = ?";

		try {
			statement = jdbc_connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

//	public ArrayList<DataFeildList> searchforLabelUserTable(int clientID, String myUserLabelName) {
//		String sql = "SELECT * FROM " + "UserTable" + " WHERE " + myUserLabelName + " = ?";
//		// String sql = "SELECT * FROM " + "UserTable" + " WHERE "+
//		// data.get(myUserLabel).getData() + " = ?";
//		ResultSet myClient;
//		try {
//			statement = jdbc_connection.prepareStatement(sql);
//			statement.setInt(1, clientID);
//			myClient = statement.executeQuery();
//			ArrayList<DataFeildList> userTableMatch = new ArrayList<DataFeildList>();
//
//			while (myClient.next()) {
//				// CourseLabel courseLabel_id = CourseLabel.ID;
//				// String = data.get(courseLabel_id).getData();
//				DataFeildList dList = new DataFeildList();
//				DataFeild myID = new DataFeild(UserLabel.ID, Integer.toString(myClient.getInt("ID")));
//				dList.add(myID);
//				DataFeild myPASSWORD = new DataFeild(UserLabel.PASSWORD, myClient.getString("PASSWORD"));
//				dList.add(myPASSWORD);
//				DataFeild myEMAIL = new DataFeild(UserLabel.EMAIL, myClient.getString("EMAIL"));
//				dList.add(myEMAIL);
//				DataFeild myFIRSTNAME = new DataFeild(UserLabel.FIRSTNAME, myClient.getString("FIRSTNAME"));
//				dList.add(myFIRSTNAME);
//				DataFeild myLASTNAME = new DataFeild(UserLabel.LASTNAME, myClient.getString("LASTNAME"));
//				dList.add(myLASTNAME);
//				DataFeild myTYPE = new DataFeild(UserLabel.TYPE, myClient.getString("TYPE"));
//				dList.add(myTYPE);
//
//				userTableMatch.add(dList);
//			}
//
//			return userTableMatch;
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return null;
//	}

}
