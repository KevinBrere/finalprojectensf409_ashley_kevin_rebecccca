package view.student;

import javax.swing.JPanel;

import view.ListDataElement;
import view.ResultsList;


/**
 * UNUSED
 * Setup if Time???
 * The Class GradesListPanel.
 * (To display all grades for a course)
 * @author Ashley
 */
public class GradesListPanel extends JPanel{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The assignment grades. */
	private ResultsList<ListDataElement> assignmentGrades;
	
	/**
	 * Instantiates a new grades list panel.
	 */
	public GradesListPanel() {
		assignmentGrades = new ResultsList<ListDataElement>();
		add(assignmentGrades);
	}
	

}
