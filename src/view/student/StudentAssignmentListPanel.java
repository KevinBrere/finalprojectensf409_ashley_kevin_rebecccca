package view.student;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import orders.guiOrder.ChangePanelOrder;
import view.AssignmentListPanel;
import view.EmailWindow;
import view.ListDataElementAssignment;
import view.ListDataElementCourse;
import view.ResultsList;
import view.RouterData;
import view.View;
import view.professor.AddAssignmentWindow;
import view.professor.panelNames;


/**
 * The Class StudentAssignmentListPanel.
 * Displays a list of all active assignments in a specified course.
 * @author Ashley
 */
public class StudentAssignmentListPanel extends AssignmentListPanel implements StudentPanel, DataSender, DataReceiver{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1826206418397049004L;
	
	/** The return to course list button. */
	private JButton returnCourse;
	
	/** The refresh button. */
	private JButton refresh;
	
	/** The chatroom. */
	private JButton chatroom;
	
	/** The router data. */
	private RouterData routerData;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The ids. 
	 * 
	 * ids[0] = studentID
	 * ids[1] = courseID
	 */
	String[] ids;
	
	
	/**
 * Instantiates a new student assignment list panel.
 *
 * @param data the router
 * @param p the p
 */
public StudentAssignmentListPanel(RouterData data, ClientPackager p) {
		
		super();
		routerData = data;
		packager=p;
		
		returnCourse = new JButton("Return to Course List");
		returnCourse.addActionListener(new ReturnActionListener());
		
		refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setup(ids);
			}
		});
		
		chatroom = new JButton("Open Chatroom");
		chatroom.addActionListener(new ChatroomListener());
		
		JPanel south = new JPanel();
		south.setBackground(View.STUDENT_BACKGROUND_COLOR);
		south.add(refresh);
		south.add(returnCourse);
		south.add(chatroom);
		add(south, layout.SOUTH);
		assignments.addListSelectionListener(new AssignmentListListener());
		
		super.addSendEmailActionListener(new EmailActionListener());
	}

	/* (non-Javadoc)
	 * @see view.student.StudentPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;
		
		
		
//		setup list by searching course for assignments
		assignments.clear();
		packager.clearReceivers();
		packager.addReceiver(this);
		
		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);
		
	}
	
	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();
		list.add(new DataFeild(AssignmentLabel.COURSE_ID, ids[1]));
		return list;
	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient message) {
		ArrayList<DataFeildList> list = message.getList();
		for (DataFeildList element : list) {
			String active = element.get(AssignmentLabel.ACTIVE).getData();
			if(active.equalsIgnoreCase("t")) {
				ListDataElementAssignment assignmentFound = new ListDataElementAssignment(element);
				assignments.addEntry(assignmentFound);
			}
		
		}
		
	}
	
	/**
	 * The listener interface for receiving assignmentList events.
	 * The class that is interested in processing a assignmentList
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addAssignmentListListener<code> method. When
	 * the assignmentList event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see AssignmentListEvent
	 */
	class AssignmentListListener implements ListSelectionListener, panelNames {

		/* (non-Javadoc)
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {

			int index = assignments.getListOfResults().getSelectedIndex();
			if (index != -1) {
				ListDataElementAssignment selected = (ListDataElementAssignment) assignments.getListModel()
						.getElementAt(index);
				String assignmentID = selected.getID();
				String[] sendData = { ids[0], ids[1], assignmentID };

				ChangePanelOrder order = new ChangePanelOrder(DETAILS, sendData);
				routerData.getGuirouter().executeOrder(order);
			}
		}

	}


	/**
	 * 
	 * Opens CourseList Panel. 
	 * 
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
		class ReturnActionListener implements ActionListener, panelNames {
			
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] courseData = { ids[0] };
				ChangePanelOrder order = new ChangePanelOrder(COURSES, courseData);
				routerData.getGuirouter().executeOrder(order);
			}

		}
		
	/**
	 * The listener interface for receiving chatroom events.
	 * The class that is interested in processing a chatroom
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addChatroomListener<code> method. When
	 * the chatroom event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ChatroomEvent
	 */
	class ChatroomListener implements ActionListener, panelNames {
			
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				String[] courseData = { ids[0] };
				ChangePanelOrder order = new ChangePanelOrder(CHATROOM, ids);
				routerData.getGuirouter().executeOrder(order);
			}

		}

	/**
	 * The listener interface for receiving emailAction events.
	 * The class that is interested in processing a emailAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addEmailActionListener<code> method. When
	 * the emailAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see EmailActionEvent
	 */
	class EmailActionListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			// System.out.println("new email clicked");
//			NewEmailOrder order = new NewEmailOrder();
//			routerData.getGuirouter().executeOrder(order);
//			String[] studentData = { profID, courseID };
			EmailWindow email = new EmailWindow(routerData, packager, ids, "S");
		}

	}
}

