package view.student;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DownloadAssignmentFromServerGUIOrder;
import control.dataGUIOrders.DownloadFileFromServerGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import model.labels.StudentEnrollmentLabel;
import model.labels.SubmissionLabel;
import orders.guiOrder.ChangePanelOrder;
import view.Field;
import view.RouterData;
import view.View;
import view.professor.panelNames;


/**
 * The Class StudentAssignmentDetails.
 * Displays the details and actions for a specific assignment.
 * @author Ashley
 */
public class StudentAssignmentDetails extends JPanel implements StudentPanel, DataReceiver{
	
	/** The gbc. */
	private GridBagConstraints gbc;
	
	/** The submit. */
	private JButton submit;
	/** The grade. */
	private Field grade;
	/** The due date. */
	private Field dueDate;
	/** The comment label. */
	private Label commentLabel;
	/** The comment text. */
	private JTextArea commentText;
	/** The download. */
	private JButton download;
	/** The return to assignments. */
	private JButton returnToAssignments;
	
	/** The router data. */
	private RouterData routerData;
	/** The packager. */
	private ClientPackager packager;
	/** The ids.
	 * 	ids[0] = studentID
	 * ids[1] = courseID
	 * ids[2] = assignmentID
	 *  
	 */
	String[] ids;

	
/**
 * Instantiates a new student assignment details.
 *
 * @param data the router
 * @param p the packager
 */
public StudentAssignmentDetails(RouterData data, ClientPackager p) {
		this.setBackground(View.STUDENT_BACKGROUND_COLOR);
		this.setLayout(new GridBagLayout ());
		gbc = new GridBagConstraints();
		
		routerData = data;
		packager=p;
		
		submit = new JButton ("Submit");
		submit.addActionListener(new NewSubmissionListener());
		
		download = new JButton("Download Assignment");
		download.addActionListener(new DownloadListener());
		
		grade = new Field("Grade: ");
		grade.getText().setEditable(false);
		
		commentLabel = new Label("Comments: ");
		commentText = new JTextArea();
		commentText.setEditable(false);
		commentText.setColumns(25);
		commentText.setRows(5);
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		commentText.setBorder(loweredbevel);
		
		dueDate = new Field("Due Date: ");
		dueDate.getText().setEditable(false);
		
		returnToAssignments = new JButton("Return to List of Assignments");
		returnToAssignments.addActionListener(new ReturnActionListener());
		
		addComponents();

	}
	
	/**
	 * Adds the components.
	 */
	public void addComponents() {
		int row = 0;
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		
		
		gbc.gridy = row++;
		gbc.gridx = 0;
		add(submit, gbc);
		gbc.gridx = 1;
		add(download, gbc);
		gbc.gridx = 2;
		add(returnToAssignments, gbc);
		
		gbc.gridy = row++;
		gbc.gridx = 0;
		add(grade.getLabel(), gbc);
		gbc.gridx = 1;
		add(grade.getText(), gbc);
		
		gbc.gridy = row++;
		row++;
		gbc.gridx = 0;
		add(commentLabel, gbc);
		gbc.gridx = 1;
		gbc.gridheight = 2;
		add(commentText, gbc);
		gbc.gridheight = 1;
		
		gbc.gridy = row++;
		gbc.gridx = 0;
		add(dueDate.getLabel(), gbc);
		gbc.gridx = 1;
		add(dueDate.getText(), gbc);
	}

	/* (non-Javadoc)
	 * @see view.student.StudentPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;
		
		grade.getText().setText("");
		commentText.setText("");
		
		setupGrade();
		SetupDueDate sdd = new SetupDueDate();
	}
	
	/**
	 * Setup grade and comment.
	 * 
	 */
	public void setupGrade() {
		packager.clearReceivers();
		packager.addReceiver(this);
		
		DataFeildList list=new DataFeildList();
		list.add(new DataFeild(SubmissionLabel.ASSIGNMENT_ID, ids[2]));
		list.add(new DataFeild(SubmissionLabel.STUDENT_ID, ids[0]));
		
		SearchGUIOrder search = new SearchGUIOrder(list, packager);
		routerData.getGuirouter().executeOrder(search);
	}
	
	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient list) {
		ArrayList<DataFeildList> submissions = list.getList();
		if(!submissions.isEmpty()) {
//			int last = list.getList().size();
			for(DataFeildList sub : submissions) {
				String currentGrade = sub.get(SubmissionLabel.SUBMISSION_GRADE).getData();
				grade.getText().setText(currentGrade);
				
				String currentComment = sub.get(SubmissionLabel.COMMENTS).getData();
				commentText.setText(currentComment);
			}
			
		}

	}
	
	/**
	 * opens the window to add a new submission
	 * 
	 * 
	 * The listener interface for receiving newSubmission events.
	 * The class that is interested in processing a newSubmission
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addNewSubmissionListener<code> method. When
	 * the newSubmission event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see NewSubmissionEvent
	 */
	class NewSubmissionListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] data = { ids[0], ids[1], ids[2] };
			new AddSubmissionWindow(routerData, packager, data);
		}
	}
	
	/**
	 * Open list of assignments.
	 * 
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	// opens CourseListPanel
	class ReturnActionListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] assignmentData = {ids[0], ids[1]  };
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, assignmentData);
			routerData.getGuirouter().executeOrder(order);

		}

	}
	
	/**
	 * Downloads the Assignment.
	 * 
	 * The listener interface for receiving download events.
	 * The class that is interested in processing a download
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addDownloadListener<code> method. When
	 * the download event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see DownloadEvent
	 */
	class DownloadListener implements ActionListener, DataSender, DataReceiver{

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			
//			search for file title
			SearchGUIOrder search = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(search);
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			// TODO Auto-generated method stub
			Download runDownload = new Download(list);
			
		}

		/* (non-Javadoc)
		 * @see control.dataSending.DataSender#send()
		 */
		@Override
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(AssignmentLabel.ID, ids[2]));
//			list.add(new DataFeild(CourseLabel.PROF_ID, profID));

			return list;
		}
		
		/**
		 * The Class Download.
		 */
		class Download implements DataReceiver{
			
			/**
			 * Instantiates a new download.
			 *
			 * @param message the message
			 */
			public Download(MessageForClient message){
				System.out.println("download started");
				packager.clearReceivers();
				packager.addReceiver(this);
				
				DataFeildList list = new DataFeildList();
				String title = message.getList().get(0).get(AssignmentLabel.TITLE).getData();
				list.add(new DataFeild(AssignmentLabel.TITLE, title));
				list.add(new DataFeild(AssignmentLabel.PATH, "C:\\\\Users\\\\Ashley\\\\Desktop\\\\" + title));
//				list.add(new DataFeild(CourseLabel.PROF_ID, profID));
				
				DownloadAssignmentFromServerGUIOrder download = new DownloadAssignmentFromServerGUIOrder(list, packager);
				routerData.getGuirouter().executeOrder(download);
			}

			/* (non-Javadoc)
			 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
			 */
			@Override
			public void receive(MessageForClient list) {
				JOptionPane.showMessageDialog(null, "Submission Downloaded", "Message", JOptionPane.PLAIN_MESSAGE );
				
			}

			
			
		}
		
	}
	


	/**
	 * The Class SetupDueDate.
	 */
	class SetupDueDate implements DataReceiver{
		
		/**
		 * Instantiates a new setup due date.
		 */
		public SetupDueDate() {
			packager.clearReceivers();
			packager.addReceiver(this);
//			
			DataFeildList list=new DataFeildList();
			list.add(new DataFeild(AssignmentLabel.ID, ids[2]));
//			list.add(new DataFeild(SubmissionLabel.STUDENT_ID, ids[0]));
//			
			SearchGUIOrder search = new SearchGUIOrder(list, packager);
			routerData.getGuirouter().executeOrder(search);
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			
			ArrayList<DataFeildList> assignments = list.getList();
			if(!assignments.isEmpty()) {
				String currentDueDate = list.getList().get(0).get(AssignmentLabel.DUE_DATE).getData();
				dueDate.getText().setText(currentDueDate);
			}

		}
	}

//	
}
