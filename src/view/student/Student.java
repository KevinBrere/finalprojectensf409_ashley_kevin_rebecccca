package view.student;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ItemEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import control.dataSending.ClientPackager;
import view.ChatRoomPanel;
import view.EmailWindow;
import view.RouterData;
import view.professor.panelNames;


/**
 * The Class Student.
 * Holds all the panels that will be viewed by a student.
 * @author Ashley
 */
public class Student extends JPanel implements panelNames{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The assignments. */
	private StudentAssignmentListPanel assignments;
	
	/** The courses. */
	private StudentCourseListPanel courses;

/** The details. */
//	private GradesListPanel grades;
	private StudentAssignmentDetails details;
	
	/** The chatroom. */
	private ChatRoomPanel chatroom;
	
	/** The data. */
	private RouterData data;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The student ID. */
	private String studentID;
	
	/**
	 * Instantiates a new student.
	 *
	 * @param pane the pane
	 * @param data the data
	 * @param studentID the student ID
	 * @param clientPackager the client packager
	 */
	public Student(Container pane, RouterData data, String studentID, ClientPackager clientPackager) {
		this.data = data;
		this.studentID = studentID;
		this.packager=clientPackager;
		
		this.setLayout(new CardLayout());
		
		assignments = new StudentAssignmentListPanel(data, packager);
		assignments.setName(ASSIGNMENTS);
		
		courses = new StudentCourseListPanel(data, packager);
		courses.setName(COURSES);
		
//		grades = new GradesListPanel();
//		grades.setName(GRADES);
		
		details = new StudentAssignmentDetails(data, packager);
		details.setName(DETAILS);
		
		chatroom = new ChatRoomPanel(data, packager);
		chatroom.setName(CHATROOM);
		
		add(assignments, ASSIGNMENTS);
		add(courses, COURSES);
//		add(grades, GRADES);
		add(details, DETAILS);
		add(chatroom, CHATROOM);
		
		pane.add(this);
		
		String [] sendStudentID = {studentID};
		changePanelDisplayed(COURSES, sendStudentID);
	}
	
	/**
	 * Change panel displayed.
	 *
	 * @param evt the evt
	 */
	public void changePanelDisplayed(ItemEvent evt) {
	    CardLayout cl = (CardLayout)(this.getLayout());
	    cl.show(this, (String)evt.getItem());
	}
	
	/**
	 * Change panel displayed.
	 *
	 * @param panel the panel
	 * @param setupData the setup data
	 */
	public void changePanelDisplayed(String panel, String[] setupData) {
//		System.out.println(this.getLayout());
	    CardLayout cl = (CardLayout)(this.getLayout());
	    cl.show(this, panel);
	    
	    StudentPanel currentPanel = (StudentPanel) getPanelByName(panel);
	    currentPanel.setup(setupData);
	}
	
	/**
	 * Gets the panel by name.
	 *
	 * @param name the name
	 * @return the panel by name
	 */
	private JPanel getPanelByName(String name) {
		if(name.equals(COURSES)) {
			return courses;
		}else if(name.equals(ASSIGNMENTS)) {
			return assignments;
//		}else if(name.equals(GRADES)) {
//			return grades;
		}else if(name.equals(DETAILS)) {
			return details;
		}else if(name.equals(CHATROOM)) {
			return chatroom;
		}
		JOptionPane.showMessageDialog(null, "error with panel names ", "Error", JOptionPane.PLAIN_MESSAGE );
		return null;
	}
	
//	public void newEmail() {
//		EmailWindow email = new EmailWindow();
//	}

}
