package view.professor;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;
import model.labels.SubmissionLabel;
import orders.guiOrder.ChangePanelOrder;
import view.ListDataElement;
import view.ListDataElementCourse;
import view.ListDataElementSubmission;
import view.ResultsList;
import view.RouterData;
import view.View;


/**
 * The Class StudentSubmissionListPanel.
 * Holds a list of all student submissions for a particular assignment.
 * @author Ashley
 */
public class StudentSubmissionListPanel extends JPanel implements ProfessorPanel, DataReceiver, DataSender {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The layout. */
	private BorderLayout layout;
	
	/** The submissions. */
	private ResultsList<ListDataElementSubmission> submissions;
	
	/** The return to assignment. */
	private JButton returnToAssignment;
	
	/** The refresh. */
	private JButton refresh;
	
	/** The south. */
	private JPanel south;

	/** The router data. */
	private RouterData routerData;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The ids. */
	private String[] ids;
	// ids[0] = profid
	// ids[1] = course id
	// ids[2] = assignment id

	/**
	 * Instantiates a new student submission list panel.
	 *
	 * @param data the data
	 * @param p the p
	 */
	public StudentSubmissionListPanel(RouterData data, ClientPackager p) {
		this.setBackground(View.BACKGROUND_COLOUR);
		layout = new BorderLayout();
		this.setLayout(layout);
		routerData = data;
		packager = p;

		submissions = new ResultsList<ListDataElementSubmission>();
		submissions.addListSelectionListener(new ListActionListener());

		returnToAssignment = new JButton("Return to Assignment Details");
		returnToAssignment.addActionListener(new ReturnActionListener());

		refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setup(ids);
			}
		});

		south = new JPanel();
		south.setBackground(View.BACKGROUND_COLOUR);

		add(submissions, layout.CENTER);

		BufferedImage myPicture;
		try {
			myPicture = ImageIO.read(new File("images\\heart1.png"));
			JLabel picLabel = new JLabel(new ImageIcon(myPicture.getScaledInstance(40, 40, Image.SCALE_FAST)));
			
			south.add(picLabel);
		} catch (IOException e) {
			e.printStackTrace();
		}
		south.add(returnToAssignment);
		south.add(refresh);
		add(south, layout.SOUTH);
	}

	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;
		submissions.clear();

		packager.clearReceivers();
		packager.addReceiver(this);
		// System.out.println("setup");
		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);

	}

	/* (non-Javadoc)
	 * @see control.dataSending.DataSender#send()
	 */
	@Override
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();
		list.add(new DataFeild(SubmissionLabel.ASSIGNMENT_ID, ids[2]));

		return list;
	}

	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient message) {
		// System.out.println("received");
		ArrayList<DataFeildList> list = message.getList();
		for (DataFeildList element : list) {
			ListDataElementSubmission courseFound = new ListDataElementSubmission(element);
			System.out.println(courseFound);
			submissions.addEntry(courseFound);
		}

	}

	/**
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	class ReturnActionListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ChangePanelOrder order = new ChangePanelOrder(DETAILS, ids);
			routerData.getGuirouter().executeOrder(order);
		}
	}

	/**
	 * The listener interface for receiving listAction events.
	 * The class that is interested in processing a listAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addListActionListener<code> method. When
	 * the listAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ListActionEvent
	 */
	class ListActionListener implements ListSelectionListener, panelNames {

		/* (non-Javadoc)
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			int index = submissions.getListOfResults().getSelectedIndex();
			if (index != -1) {
				ListDataElementSubmission selectedCourse = (ListDataElementSubmission) submissions.getListModel()
						.getElementAt(index);
				String submissionID = selectedCourse.getID();

				String[] setup = { ids[0], ids[1], ids[2], submissionID };
				// courses.clear();
				ChangePanelOrder order = new ChangePanelOrder(SUBMISSION, setup);
				routerData.getGuirouter().executeOrder(order);
			}
		}
	}
}
