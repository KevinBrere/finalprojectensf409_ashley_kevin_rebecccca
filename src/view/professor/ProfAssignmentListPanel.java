package view.professor;

import java.awt.BorderLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.MessageForClient;
import control.clientDataReceiving.Router;
import control.dataGUIOrders.ModifyGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.datafeildStuff.UserType;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import model.labels.UserLabel;
import orders.guiOrder.ChangePanelOrder;
//import orders.guiOrder.NewEmailOrder;
//import orders.guiOrder.NewEmailOrder;
import view.AssignmentListPanel;
import view.EmailWindow;
import view.ListDataElement;
import view.ListDataElementAssignment;
import view.ListDataElementCourse;
import view.RouterData;
import view.View;



/**
 * The Class ProfAssignmentListPanel.
 * The home page for a course that belongs to the professor.
 * @author Ashley
 */
public class ProfAssignmentListPanel extends AssignmentListPanel implements DataReceiver, ProfessorPanel {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2675402198645932922L;

	/** The south. */
	private JPanel south;

	/** The change student enrollment. */
	private JButton changeStudentEnrollment;
	
	/** The active. */
	private CheckBox active;
	
	/** The chatroom. */
	private JButton chatroom;
	
	/** The add assignment. */
	private JButton addAssignment;
	
	/** The return to courses. */
	private JButton returnToCourses;
	
	/** The course. */
	private Title course;

	// private GUIRouter guirouter;
	// private Router router;

	/** The router data. */
	private RouterData routerData;
	
	/** The prof ID. */
	private String profID;
	
	/** The course ID. */
	private String courseID;
	
	/** The course name string. */
	private String courseNameString;

	/** The packager. */
	private ClientPackager packager;

	/**
	 * Instantiates a new prof assignment list panel.
	 *
	 * @param data the data
	 * @param packager the packager
	 */
	public ProfAssignmentListPanel(RouterData data, ClientPackager packager) {

		super();
		this.setBackground(View.BACKGROUND_COLOUR);
		sendEmail.addActionListener(new EmailActionListener());
		assignments.addListSelectionListener(new AssignmentListListener());

		south = new JPanel();
		south.setBackground(View.BACKGROUND_COLOUR);

		this.packager = packager;
		routerData = data;
		// course = new Label("ProfAssignmentListPanel");
		changeStudentEnrollment = new JButton("Change Enrollment");
		changeStudentEnrollment.addActionListener(new EnrollmentActionListener());

		active = new CheckBox();
		active.getActive().addActionListener(new ActiveListener());
		
		chatroom = new JButton("Open Chatroom");
		chatroom.addActionListener(new ChatroomListener());

		// viewSubmissions = new JButton("View Submissions");

		addAssignment = new JButton("Add Assignment");
		addAssignment.addActionListener(new NewAssignmentListener());

		returnToCourses = new JButton("Return To Courses");
		returnToCourses.addActionListener(new ReturnActionListener());

		// add(course);

		south.add(sendEmail);
		south.add(changeStudentEnrollment);
		south.add(active.getActive());
		south.add(addAssignment);
		south.add(super.sendEmail);
		south.add(chatroom);
		south.add(returnToCourses);
		

		add(south, layout.SOUTH);

		course = new Title();
		add(course.label, layout.NORTH);

	}

	// needs to run when assignment list is the visible panel
	// need data about course that the assignments belong to

	// data[0] = profID
	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	// data[1] = courseID
	public void setup(String[] data) {
		// System.out.println("Assignment list panel setup started");

		this.profID = data[0];
		this.courseID = data[1];
		//
		// ListDataElementAssignment [] assignmentsFound;
		assignments.clear();
		packager.clearReceivers();
		packager.addReceiver(this);

		SearchGUIOrder search = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(search);

		course.setup();
		active.setup();

	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active.getActive().isSelected();
	}

	// // Getters
	// public JButton getAddStudent() {
	// return changeStudentEnrollment;
	// }
	//
	// public JButton getAddAssignment() {
	// return addAssignment;
	// }
	//
	// public JButton getReturnToCourses() {
	// return returnToCourses;
	// }
	/**
	 * Send.
	 *
	 * @return the data feild list
	 */
	//
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();
		list.add(new DataFeild(AssignmentLabel.COURSE_ID, courseID));
		return list;
	}

	// Receives data from Listener in ProfCourseListPanel and sets up
	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	// ProfAssignmentListPanel
	@Override
	public void receive(MessageForClient message) {
		System.out.println(message);
		ArrayList<DataFeildList> list = message.getList();
		for (DataFeildList element : list) {
			ListDataElementAssignment assignmentFound = new ListDataElementAssignment(element);
			assignments.addEntry(assignmentFound);
		}

	}

	/**
	 * The listener interface for receiving assignmentList events.
	 * The class that is interested in processing a assignmentList
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addAssignmentListListener<code> method. When
	 * the assignmentList event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see AssignmentListEvent
	 */
	// opens assignment Details window
	class AssignmentListListener implements ListSelectionListener, panelNames {

		/* (non-Javadoc)
		 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {

			int index = assignments.getListOfResults().getSelectedIndex();
			if (index != -1) {
				ListDataElementAssignment selected = (ListDataElementAssignment) assignments.getListModel()
						.getElementAt(index);
				String assignmentID = selected.getID();
				String[] sendData = { profID, courseID, assignmentID };

				ChangePanelOrder order = new ChangePanelOrder(DETAILS, sendData);
				routerData.getGuirouter().executeOrder(order);

			}
			// ListDataElementAssignment selectedCourse =
			// (ListDataElementAssignment)e.getSource();

			// search for contents of panel
			// setup panel

		}

	}

	/**
	 * The listener interface for receiving emailAction events.
	 * The class that is interested in processing a emailAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addEmailActionListener<code> method. When
	 * the emailAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see EmailActionEvent
	 */
	// opens NewEmailWindow
	class EmailActionListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			// System.out.println("new email clicked");
//			NewEmailOrder order = new NewEmailOrder();
//			routerData.getGuirouter().executeOrder(order);
			String[] studentData = { profID, courseID };
			EmailWindow email = new EmailWindow(routerData, packager, studentData, "P");
		}

	}

	/**
	 * The listener interface for receiving enrollmentAction events.
	 * The class that is interested in processing a enrollmentAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addEnrollmentActionListener<code> method. When
	 * the enrollmentAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see EnrollmentActionEvent
	 */
	// opens StudentListPanel
	class EnrollmentActionListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] studentData = { profID, courseID };
			ChangePanelOrder order = new ChangePanelOrder(STUDENTS, studentData);
			routerData.getGuirouter().executeOrder(order);

		}

	}

	/**
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	// opens CourseListPanel
	class ReturnActionListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] courseData = { profID };
			ChangePanelOrder order = new ChangePanelOrder(COURSES, courseData);
			routerData.getGuirouter().executeOrder(order);

		}

	}

	/**
	 * The listener interface for receiving newAssignment events.
	 * The class that is interested in processing a newAssignment
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addNewAssignmentListener<code> method. When
	 * the newAssignment event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see NewAssignmentEvent
	 */
	class NewAssignmentListener implements ActionListener {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] data = { profID, courseID };
			new AddAssignmentWindow(routerData, packager, data);
		}
	}

	/**
	 * The listener interface for receiving active events.
	 * The class that is interested in processing a active
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addActiveListener<code> method. When
	 * the active event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ActiveEvent
	 */
	class ActiveListener implements ActionListener, DataSender, DataReceiver {

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder order = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(order);
			//
		}

		/* (non-Javadoc)
		 * @see control.dataSending.DataSender#send()
		 */
		@Override
		public DataFeildList send() {

			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(CourseLabel.ID, courseID));
			list.add(new DataFeild(CourseLabel.PROF_ID, profID));

			return list;
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient message) {
			System.out.println("update active");
			DataFeildList course = message.getList().get(0);
			course.delete(CourseLabel.ACTIVE);

			boolean isActive = active.getActive().isSelected();
			String active;
			if (isActive) {
				active = "t";
			} else {
				active = "f";
			}
			course.add(new DataFeild(CourseLabel.ACTIVE, active));

			packager.clearReceivers();
			ModifyGUIOrder update = new ModifyGUIOrder(course, packager);
			routerData.getGuirouter().executeOrder(update);
		}

	}

	/**
	 * The Class Title.
	 */
	class Title implements DataReceiver {
		
		/** The label. */
		private Label label;

		/**
		 * Instantiates a new title.
		 */
		public Title() {
			label = new Label("Loading");

		}

		/**
		 * Setup.
		 */
		public void setup() {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder order = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(order);
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient message) {
			String courseName = message.getList().get(0).get(CourseLabel.NAME).getData();
			label.setText(courseName);
			courseNameString = courseName;

		}

		/**
		 * Send.
		 *
		 * @return the data feild list
		 */
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();

			list.add(new DataFeild(CourseLabel.PROF_ID, profID));
			list.add(new DataFeild(CourseLabel.ID, courseID));
			return list;
		}
	}

	/**
	 * The Class CheckBox.
	 */
	class CheckBox implements DataReceiver {

		/** The active. */
		private JCheckBox active;

		/**
		 * Instantiates a new check box.
		 */
		public CheckBox() {
			active = new JCheckBox("active");
			active.setBackground(View.BACKGROUND_COLOUR);
		}

		/**
		 * Setup.
		 */
		public void setup() {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder search = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(search);
		}

		/**
		 * Send.
		 *
		 * @return the data feild list
		 */
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();

			list.add(new DataFeild(CourseLabel.PROF_ID, profID));
			list.add(new DataFeild(CourseLabel.ID, courseID));
			return list;
		}

		/* (non-Javadoc)
		 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
		 */
		@Override
		public void receive(MessageForClient list) {
			String isActive = list.getList().get(0).get(CourseLabel.ACTIVE).getData();
			if (isActive.equalsIgnoreCase("t")) {
				active.setSelected(true);
			} else if (isActive.equalsIgnoreCase("f")) {
				active.setSelected(false);
			} else {
				// System.out.println("Error reading whether course if active");
				JOptionPane.showMessageDialog(null, "Error reading whether course is active", "Error",
						JOptionPane.PLAIN_MESSAGE);
			}
		}

		/**
		 * Gets the active.
		 *
		 * @return the active
		 */
		public JCheckBox getActive() {
			return active;
		}

	}
	
	
	/**
	 * The listener interface for receiving chatroom events.
	 * The class that is interested in processing a chatroom
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addChatroomListener<code> method. When
	 * the chatroom event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ChatroomEvent
	 */
	class ChatroomListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] ids = {profID, courseID };
			ChangePanelOrder order = new ChangePanelOrder(CHATROOM, ids);
			routerData.getGuirouter().executeOrder(order);
		}

	}

}
