package view.professor;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ItemEvent;

import javax.swing.JPanel;

import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.Router;
import control.dataSending.ClientPackager;
import view.ChatRoomPanel;
import view.EmailWindow;
import view.RouterData;


/**
 * The Class Professor.
 * Holds the panels that can be viewed by a professor.
 * @author Ashley
 */
public class Professor extends JPanel implements panelNames {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The courses. */
	private ProfCourseListPanel courses;
	
	/** The assignments. */
	private ProfAssignmentListPanel assignments;
	
	/** The assignment details. */
	private AssignmentDetails assignmentDetails;
	
	/** The students. */
	private StudentListPanel students;
	
	/** The assignment submissions. */
	private StudentSubmissionListPanel assignmentSubmissions;
	
	/** The submission. */
	private DetailedStudentSubmissionPanel submission;
	
	/** The chatroom. */
	private ChatRoomPanel chatroom;
	// private EmailWindow email;

	/** The data. */
	private RouterData data;
	
	/** The packager. */
	ClientPackager packager;
	
	/** The prof ID. */
	private String profID;

	/**
	 * Instantiates a new professor.
	 *
	 * @param pane the pane
	 * @param data the data
	 * @param profID the prof ID
	 * @param clientPackager the client packager
	 */
	public Professor(Container pane, RouterData data, String profID, ClientPackager clientPackager) {
		this.data = data;
		this.profID = profID;
		this.packager = clientPackager;

		// System.err.println("setting layout");
		try {
			this.setLayout(new CardLayout());
		} catch (Exception e) {
			System.out.println("Error thrown in setting layout");
		}

		// System.err.println("setting up panels");
		try {
			setupPanels();
		} catch (Exception e) {
			System.out.println("Error thrown in setupPanels()");
		}

		try {
			addPanels();
		} catch (Exception e) {
			System.out.println("Error thrown in addPanels()");
		}

		try {
			pane.add(this);
		} catch (Exception e) {
			System.out.println("Error thrown when adding pane");
		}

		String[] sendProfID = new String[1];
		sendProfID[0] = profID;
		// display courses to start
		changePanelDisplayed(COURSES, sendProfID);

	}

	/**
	 * Setup panels.
	 *
	 * @throws Exception the exception
	 */
	private void setupPanels() throws Exception {
		courses = new ProfCourseListPanel(data, packager);
		courses.setName(COURSES);

		assignments = new ProfAssignmentListPanel(data, packager);
		assignments.setName(ASSIGNMENTS);

		students = new StudentListPanel(data, packager);
		students.setName(STUDENTS);

		assignmentDetails = new AssignmentDetails(data, packager);
		assignmentDetails.setName(DETAILS);

		assignmentSubmissions = new StudentSubmissionListPanel(data, packager);
		assignmentSubmissions.setName(ASSIGNMENTSUBMISSIONS);

		submission = new DetailedStudentSubmissionPanel(data, packager);
		submission.setName(SUBMISSION);
		
		chatroom = new ChatRoomPanel(data, packager);
		chatroom.setName(CHATROOM);
	}

	/**
	 * Adds the panels.
	 *
	 * @throws Exception the exception
	 */
	private void addPanels() throws Exception {
		this.add(courses, COURSES);
		this.add(assignments, ASSIGNMENTS);
		this.add(assignmentDetails, DETAILS);
		this.add(students, STUDENTS);
		this.add(assignmentSubmissions, ASSIGNMENTSUBMISSIONS);
		this.add(submission, SUBMISSION);
		this.add(chatroom, CHATROOM);
	}

//	public void newEmail() {
//		EmailWindow email = new EmailWindow();
//
//	}

	// public void changePanelDisplayed(ItemEvent evt) {
	// CardLayout cl = (CardLayout)(this.getLayout());
	// cl.show(this, (String)evt.getItem());
	//
	//
	//
	// }

	/**
 * Change panel displayed.
 *
 * @param panel the panel
 * @param setupData the setup data
 */
public void changePanelDisplayed(String panel, String[] setupData) {
		// System.out.println(panel);
		CardLayout cl = (CardLayout) (this.getLayout());

		cl.show(this, panel);
		ProfessorPanel currentPanel = (ProfessorPanel) getPanelByName(panel);
		// System.out.println("professor displayed");
		currentPanel.setup(setupData);

	}

	/**
	 * Gets the panel by name.
	 *
	 * @param name the name
	 * @return the panel by name
	 */
	private JPanel getPanelByName(String name) {
		if (name.equals(COURSES)) {
			return courses;
		} else if (name.equals(ASSIGNMENTS)) {
			return assignments;
		} else if (name.equals(ASSIGNMENTSUBMISSIONS)) {
			return assignmentSubmissions;
		} else if (name.equals(DETAILS)) {
			return assignmentDetails;
		} else if (name.equals(STUDENTS)) {
			return students;
		} else if (name.equals(SUBMISSION)) {
			return submission;
		}else if (name.equals(CHATROOM)) {
			return chatroom;
		}
		return null;
	}

	/**
	 * Gets the assignments.
	 *
	 * @return the assignments
	 */
	public ProfAssignmentListPanel getAssignments() {
		return assignments;
	}

	/**
	 * Gets the students.
	 *
	 * @return the students
	 */
	public StudentListPanel getStudents() {
		return students;
	}

	/**
	 * Gets the courses.
	 *
	 * @return the courses
	 */
	public ProfCourseListPanel getCourses() {
		return courses;
	}

	/**
	 * Gets the assignment submissions.
	 *
	 * @return the assignment submissions
	 */
	public StudentSubmissionListPanel getAssignmentSubmissions() {
		return assignmentSubmissions;
	}

	/**
	 * Gets the submission.
	 *
	 * @return the submission
	 */
	public DetailedStudentSubmissionPanel getSubmission() {
		return submission;
	}

	/**
	 * Gets the assignment details.
	 *
	 * @return the assignment details
	 */
	public AssignmentDetails getAssignmentDetails() {
		return assignmentDetails;
	}

	// public EmailWindow getEmail() {
	// return email;
	// }

}
