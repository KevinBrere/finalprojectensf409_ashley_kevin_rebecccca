package view.professor;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DownloadAssignmentFromServerGUIOrder;
import control.dataGUIOrders.DownloadFileFromServerGUIOrder;
import control.dataGUIOrders.ModifyGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataSending.ClientPackager;
import control.dataSending.DataSender;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.CourseLabel;
import model.labels.SubmissionLabel;
import orders.guiOrder.ChangePanelOrder;
import view.Field;
import view.RouterData;
import view.View;

// TODO: Auto-generated Javadoc
//import view.student.StudentAssignmentDetails.DownloadListener.Download;

/**
 * The Class DetailedStudentSubmissionPanel.
 * @author Ashley
 */
public class DetailedStudentSubmissionPanel extends JPanel implements ProfessorPanel, DataReceiver {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	/** The gbc. */
	private GridBagConstraints gbc;
	/** The row. */
	private int row;
	/** The return to submission list. */
	private JButton returnToSubmissionList;
	/** The download. */
	private JButton download;
	/** The comment label. */
	private Label commentLabel;
	/** The comment box. */
	private JTextArea commentBox;
	/** The grade. */
	private Field grade;
	/** The update grade. */
	private JButton updateGrade;
	/** The return to submissions. */
	private JButton returnToSubmissions;
	/** The router data. */
	private RouterData routerData;
	/** The packager. */
	private ClientPackager packager;

	/**
	 * The ids.
	 * 
	 * ids[0] = prof id ids[1] = course i ids[2] = assignment id ids[3] = submission
	 * id
	 * 
	 */
	private String[] ids;

	/**
	 * Instantiates a new detailed student submission panel.
	 *
	 * @param data
	 *            the data
	 * @param p
	 *            the p
	 */
	public DetailedStudentSubmissionPanel(RouterData data, ClientPackager p) {
		this.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		row = 0;

		this.setBackground(View.BACKGROUND_COLOUR);
		routerData = data;
		packager = p;

		download = new JButton("Download");
		download.addActionListener(new DownloadListener());

		commentLabel = new Label("Comments: ");
		commentBox = new JTextArea();
		commentBox.setColumns(25);
		commentBox.setRows(5);
		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		commentBox.setBorder(loweredbevel);

		grade = new Field("Grade: ");

		updateGrade = new JButton("Update Grade");
		updateGrade.addActionListener(new GradeListener());

		returnToSubmissions = new JButton("Return to list of all submissions");
		returnToSubmissions.addActionListener(new ReturnListener());

		addComponents();
	}

	/**
	 * Adds the components.
	 */
	private void addComponents() {

		gbc.gridy = row++;
		gbc.gridx = 0;
		add(download, gbc);

		gbc.gridy = row++;
		gbc.gridx = 0;
		add(grade.getLabel(), gbc);
		gbc.gridx = 1;
		add(grade.getText(), gbc);

		gbc.gridy = row++;
		gbc.gridx = 0;
		add(commentLabel, gbc);

		gbc.gridy = row;
		row += 2;
		gbc.gridx = 0;
		gbc.gridheight = 2;
		gbc.gridwidth = 2;
		add(commentBox, gbc);

		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridy = row++;
		gbc.gridx = 0;
		add(updateGrade, gbc);
		gbc.gridx = 1;
		add(returnToSubmissions, gbc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;

		setupGradeLabelAndComments();
	}

	/**
	 * Setup grade label and comments.
	 */
	private void setupGradeLabelAndComments() {
		packager.clearReceivers();
		packager.addReceiver(this);
		SearchGUIOrder order = new SearchGUIOrder(send(), packager);
		routerData.getGuirouter().executeOrder(order);
	}

	/**
	 * Creates a DataFeild with with the id of the submission for
	 * setupGradeLabelAndComments.
	 *
	 * @return the data field list
	 */
	public DataFeildList send() {
		DataFeildList list = new DataFeildList();
		list.add(new DataFeild(SubmissionLabel.ID, ids[3]));

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.
	 * MessageForClient)
	 */
	@Override
	public void receive(MessageForClient message) {

		DataFeildList course = message.getList().get(0);
		String currentGrade = course.get(SubmissionLabel.SUBMISSION_GRADE).getData();
		if (currentGrade != null) {
			grade.getText().setText(currentGrade);
		}

		String currentComment = course.get(SubmissionLabel.COMMENTS).getData();
		if (currentComment != null) {
			commentBox.setText(currentComment);
		}
	}

	/**
	 * The listener interface for receiving grade events. The class that is
	 * interested in processing a grade event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addGradeListener<code> method. When the grade event occurs,
	 * that object's appropriate method is invoked.
	 *
	 * @see GradeEvent
	 */
	class GradeListener implements ActionListener, DataReceiver {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);
			SearchGUIOrder order = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(order);
		}

		/**
		 * Replaces the grade and comment on the current submission with the contents of
		 * the grade and comment text boxes.
		 *
		 * @param message the message
		 */
		@Override
		public void receive(MessageForClient message) {
			DataFeildList course = message.getList().get(0);

			course.delete(SubmissionLabel.SUBMISSION_GRADE);
			// limits grades to 3 characters
			String newGrade = grade.getText().getText();
			if (newGrade.length() > 3) {
				newGrade = newGrade.substring(3);
			}
			course.add(new DataFeild(SubmissionLabel.SUBMISSION_GRADE, newGrade));

			course.delete(SubmissionLabel.COMMENTS);
			// limits comment to 140 characters
			String newComment = commentBox.getText();
			if (newGrade.length() > 140) {
				newComment = newComment.substring(140);
			}
			course.add(new DataFeild(SubmissionLabel.COMMENTS, newComment));

			packager.clearReceivers();
			ModifyGUIOrder update = new ModifyGUIOrder(course, packager);
			routerData.getGuirouter().executeOrder(update);
		}
	}

	/**
	 * The listener interface for receiving return events. The class that is
	 * interested in processing a return event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addReturnListener<code> method. When the return event
	 * occurs, that object's appropriate method is invoked.
	 *
	 * @see ReturnEvent
	 */
	class ReturnListener implements ActionListener, panelNames {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String[] data = { ids[0], ids[1], ids[2] };
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTSUBMISSIONS, data);
			routerData.getGuirouter().executeOrder(order);
		}
	}

	/**
	 * The listener interface for receiving download events. The class that is
	 * interested in processing a download event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addDownloadListener<code> method. When the download event
	 * occurs, that object's appropriate method is invoked.
	 *
	 * @see DownloadEvent
	 */
	class DownloadListener implements ActionListener, DataSender, DataReceiver {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			packager.clearReceivers();
			packager.addReceiver(this);

			// search for file title
			SearchGUIOrder search = new SearchGUIOrder(send(), packager);
			routerData.getGuirouter().executeOrder(search);
		}

		/**
		 * Downloads submission using the results of the search.
		 *
		 * @param list the list
		 */
		@Override
		public void receive(MessageForClient list) {
			
//			
			Download runDownload = new Download(list);
		}

		/**
		 * Creates a new DataFeildList with the ID of the submission currently
		 * displayed.
		 *
		 * @return the data feild list
		 */
		@Override
		public DataFeildList send() {
			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(SubmissionLabel.ID, ids[3]));
			return list;
		}

		/**
		 * The Class Download, used to run a DownloadFileFromServerGUIOrder.
		 */
		class Download implements DataReceiver {

			/**
			 * Instantiates a new download.
			 *
			 * @param message
			 *            the message
			 */
			public Download(MessageForClient message) {
				packager.clearReceivers();
				packager.addReceiver(this);

				DataFeildList list = new DataFeildList();
				String title = 	message.getList().get(0).get(SubmissionLabel.TITLE).getData();
				list.add(new DataFeild(SubmissionLabel.TITLE, title));
				list.add(new DataFeild(SubmissionLabel.PATH, "C:\\\\Users\\\\Ashley\\\\Desktop\\\\" + title));
				// list.add(new DataFeild(CourseLabel.PROF_ID, profID));

				DownloadFileFromServerGUIOrder download = new DownloadFileFromServerGUIOrder(list, packager);
				routerData.getGuirouter().executeOrder(download);
			}

			/**
			 * Ensures download sucessfully went through.
			 *
			 * @param list the list
			 */
			@Override
			public void receive(MessageForClient list) {
				JOptionPane.showMessageDialog(null, "Assignment Downloaded", "Message", JOptionPane.PLAIN_MESSAGE);

			}

		}

	}

}
