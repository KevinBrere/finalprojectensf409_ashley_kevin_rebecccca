package view;

import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import model.labels.SubmissionLabel;

// TODO: Auto-generated Javadoc
/**
 * The Class ListDataElementMessage.
 * @author Ashley
 */
public class ListDataElementMessage extends ListDataElement{
	
	/**
	 * Instantiates a new list data element message.
	 *
	 * @param data the data
	 */
	public ListDataElementMessage(DataFeildList data) {
//		MessageLabel message_id = MessageLabel.ID;
//		id = data.get(message_id).getData();
		
		String to = data.get(MessageLabel.TO).getData();
		String from = data.get(MessageLabel.FROM).getData();
		String message = data.get(MessageLabel.MESSAGE).getData();
		
		display = " "+ System.lineSeparator() + message + System.lineSeparator() + "      From: " + from;
		
		
	}

}
