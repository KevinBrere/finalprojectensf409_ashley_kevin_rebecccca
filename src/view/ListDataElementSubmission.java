package view;

import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import model.labels.SubmissionLabel;

// TODO: Auto-generated Javadoc
//import data.dataFeild.DataFeildList;
//import data.labels.SubmissionLabel;

/**
 * The Class ListDataElementSubmission.
 * @author Ashley
 */
public class ListDataElementSubmission extends ListDataElement{
	
	
	/**
	 * Instantiates a new list data element submission.
	 *
	 * @param data the data
	 */
	public ListDataElementSubmission(DataFeildList data) {

		SubmissionLabel submission_id = SubmissionLabel.ID;
		id = data.get(submission_id).getData();
		
		// need student name and submission name???
		SubmissionLabel submission_title = SubmissionLabel.TITLE;
		display = data.get(submission_title).getData();
		
		
	}
	

}
