package view;

// TODO: Auto-generated Javadoc
//import data.dataFeild.DataFeildList;
//import data.labels.CourseLabel;

/**
 * The Class ListDataElement.
 * @author Ashley
 */
public abstract class ListDataElement {

	/** The id. */
	protected String id;
	
	/** The display. */
	protected String display;
	

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getID() {
		return id;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return display;
	}
	
}
