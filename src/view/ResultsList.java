package view;

import java.awt.BorderLayout;
//import java.awt.Component;

//import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
//import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;


/**
 * The Class ResultsList.
 * creates a Jlist to display the search results in
 * @author Ashley
 * @version 1.2
 */
public class ResultsList <T> extends JPanel{
	
	private static final long serialVersionUID = 1L;

	/** The list of results. */
	private JList<T> listOfResults;
	
	/** The list model. */
	private DefaultListModel<T> listModel;
	
	
	/**
	 * Instantiates a new results list.
	 */
	public ResultsList() {
		super(new BorderLayout());
		listModel = new DefaultListModel<T>();
		
		listOfResults = new JList<T>(listModel);
		listOfResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listOfResults.setSelectedIndex(0);

        JScrollPane listScrollPane = new JScrollPane(listOfResults);
        add(listScrollPane);
	}
//
//	/**
//	 * Adds the client.
//	 *
//	 * @param client the client
//	 */
//	public void addClient(Client client) {
//		listModel.addElement(client);
//	}
	
	/**
	 * Adds a new entry to the list.
	 * 
	 * @param entry the entry being added
	 */
	public void addEntry(T entry) {
		listModel.addElement(entry);
	}
	
	/**
	 * Clear.
	 */
	public void clear() {
		listModel.removeAllElements();
	}

	/**
	 * Gets the list of results.
	 *
	 * @return the list of results
	 */
	public JList<T> getListOfResults() {
		return listOfResults;
	}

	/**
	 * Adds the list selection listener.
	 *
	 * @param listener the listener
	 */
	public void addListSelectionListener(ListSelectionListener listener) {
		listOfResults.addListSelectionListener(listener);
	}

	/**
	 * Gets the list model.
	 *
	 * @return the list model
	 */
	public DefaultListModel<T> getListModel() {
		return listModel;
	}
	
}