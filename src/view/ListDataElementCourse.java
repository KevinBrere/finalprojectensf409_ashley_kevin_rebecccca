package view;

import model.datafeildStuff.DataFeildList;
import model.labels.CourseLabel;

// TODO: Auto-generated Javadoc
//import data.dataFeild.DataFeildList;
//import data.labels.CourseLabel;

/**
 * The Class ListDataElementCourse.
 * @author Ashley
 */
public class ListDataElementCourse  extends ListDataElement{

	
	/**
	 * Instantiates a new list data element course.
	 *
	 * @param data the data
	 */
	public ListDataElementCourse(DataFeildList data) {
		//get contents of DataField list

		//course ID
		CourseLabel courseLabel_id = CourseLabel.ID;
		id = data.get(courseLabel_id).getData();
		
		CourseLabel courseLabel_name = CourseLabel.NAME;
		display = data.get(courseLabel_name).getData();
	}
	
	/**
	 * Instantiates a new list data element course.
	 *
	 * @param id the id
	 * @param name the name
	 */
	public ListDataElementCourse(String id, String name) {
		//get contents of DataField list

		//course ID
		this.id = id;
		this.display = name;
	}
}
