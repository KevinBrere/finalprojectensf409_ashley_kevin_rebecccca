package view;

import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.Router;


/**
 * The Class RouterData.
 * Pretty much defunct.  Just holds the GUIRouter now.
 * @author Ashley
 */
public class RouterData {
	
/** The guirouter. */
//	private Router router;
	private GUIRouter guirouter;
//	private String id;
	
//	public PanelData( GUIRouter guir, Router r,String id) {
//		router = r;
//		guirouter = guir;
//		this.id = id;
//	}
	
	/**
 * Instantiates a new router data.
 *
 * @param guir the guir
 */
public RouterData(GUIRouter guir) {
//		router = r;
		guirouter = guir;
	}

//	public Router getRouter() {
//		return router;
//	}

//	public void setRouter(Router router) {
//		this.router = router;
//	}

	/**
 * Gets the guirouter.
 *
 * @return the guirouter
 */
public GUIRouter getGuirouter() {
		return guirouter;
	}

	/**
	 * Sets the guirouter.
	 *
	 * @param guirouter the new guirouter
	 */
	public void setGuirouter(GUIRouter guirouter) {
		this.guirouter = guirouter;
	}
//
//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
	
	

}
