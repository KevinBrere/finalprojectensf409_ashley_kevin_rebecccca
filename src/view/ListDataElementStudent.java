package view;

//import data.dataFeild.DataFeildList;
import model.labels.StudentEnrollmentLabel;
//import data.labels.StudentEnrollmentLabel;
import model.labels.UserLabel;
import model.datafeildStuff.DataFeildList;

// TODO: Auto-generated Javadoc
/**
 * The Class ListDataElementStudent.
 * @author Ashley
 */
public class ListDataElementStudent extends ListDataElement{
	
	/** The first. */
	String first;
	
	/** The last. */
	String last;
	
	/** The student ID. */
	String studentID;
	
	/** The element type. */
	String elementType;

	/**
	 * Instantiates a new list data element student.
	 *
	 * @param data the data
	 */
	public ListDataElementStudent(DataFeildList data) {
//		StudentEnrollmentLabel enrollmentID = StudentEnrollmentLabel.ID;
//		id = data.get(enrollmentID).getData();
//		
//		StudentEnrollmentLabel studentName = StudentEnrollmentLabel.STUDENT_ID;
//		display = data.get(studentName).getData();
		
		if(data.get(0).getLabel().equals(UserLabel.ID)) {
			elementType = "NAME";
		
			
			UserLabel enrollmentID = UserLabel.ID;
			id = data.get(enrollmentID).getData();
			studentID = id;
			
			UserLabel studentName = UserLabel.FIRSTNAME;
			first = data.get(studentName).getData();
			
			UserLabel lastName = UserLabel.LASTNAME;
			last = data.get(lastName).getData();
			
			
	
			display = first + " " + last;
		}
		
		else if (data.get(0).getLabel().equals(StudentEnrollmentLabel.ID)) 
		{
			elementType = "ID";
			StudentEnrollmentLabel enrollmentID = StudentEnrollmentLabel.ID;
			id = data.get(enrollmentID).getData();
			
			
			
			StudentEnrollmentLabel studentName = StudentEnrollmentLabel.STUDENT_ID;
			display = data.get(studentName).getData();
			studentID = display;
		}

//		Check if student is enrolled in class displayed?
//		UserLabel studentStatus = UserLabel

		
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return elementType;
	}
	
	/**
	 * Gets the student ID.
	 *
	 * @return the student ID
	 */
	public String getStudentID() {
		return studentID;
	}
}
