package view;

/**
 * class which is here for displaying error messages to the client, in case something goes wrong in
 * a non-veiw system.  I know it does not do very much right now, but I added it in just in case we wanted
 * to customize what is shown or something.
 * @author brere
 */
public class ErrorDisplayer  {
	
	public void outputErrorMessage(String s) {
		System.out.println(s);
	}

}
