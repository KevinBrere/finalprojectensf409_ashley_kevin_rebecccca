package view;

import java.awt.BorderLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerExitRoomGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerJoinRoomGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import model.labels.MessageLabel;
import orders.guiOrder.ChangePanelOrder;
import view.professor.ProfessorPanel;
import view.professor.panelNames;
import view.student.StudentPanel;


/**
 * The Class ChatRoomPanel.
 * Displays the chatroom with a list of messages.
 * Implemented by both professor and student.
 * @author Ashley
 */
public class ChatRoomPanel extends JPanel implements ProfessorPanel, StudentPanel, DataReceiver{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1214802459678087142L;
	
	/** The layout. */
	private BorderLayout layout;
	
	/** The south. */
	private JPanel south;
	
	/** The messages. */
	private ResultsList<ListDataElementMessage> messages;
	
	/** The refresh. */
	private JButton refresh;
	
	/** The return course. */
	private JButton returnCourse;
	
	/** The new message. */
	private JButton newMessage;
	
	/** The router data. */
	private RouterData routerData;
	
	/** The packager. */
	private ClientPackager packager;
	
	/** The ids. 
	 * 
	 * ids[0] = UserID
	 * ids[1] = courseID
	 */
	String[] ids;

	/**
	 * Instantiates a new chat room panel.
	 *
	 * @param r the r
	 * @param p the p
	 */
	public ChatRoomPanel (RouterData r, ClientPackager p) {
		layout = new BorderLayout();
		south = new JPanel();
		this.setLayout(layout);
		this.setBackground(View.LOGIN_BACKGROUND_COLOUR);
		south.setBackground(View.LOGIN_BACKGROUND_COLOUR);
		
		routerData = r;
		packager = p;
		
		messages = new ResultsList<ListDataElementMessage>();
		
		refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setup(ids);
			}
		});
		
		returnCourse = new JButton("Return to Course List");
		returnCourse.addActionListener(new ReturnActionListener());
		
		newMessage = new JButton("New Message");
		newMessage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new NewMessageWindow(routerData , packager, ids);
			}
		});
		
		this.add(new Label ("Chatroom"), layout.NORTH);
		this.add(messages, layout.CENTER);
		south.add(refresh);
		south.add(returnCourse);
		south.add(newMessage);
		this.add(south, layout.SOUTH);
		
	
		
		
	}
	
	/* (non-Javadoc)
	 * @see view.professor.ProfessorPanel#setup(java.lang.String[])
	 */
	@Override
	public void setup(String[] arguments) {
		ids = arguments;
		
		packager.clearReceivers();
		packager.addReceiver(this);
		
		messages.clear();
		
//		DataFeildList list = new DataFeildList();
//		list.add(new DataFeild(MessageLabel.CLASSID, ids[1]));
//		
//		SearchGUIOrder search = new SearchGUIOrder(list, packager);
//		routerData.getGuirouter().executeOrder(search);
		setupMessenger();
	}
	

	/**
	 * Setup messenger.
	 */
	private void setupMessenger() {
		
		DataFeildList list = new DataFeildList();
		list.add(new DataFeild(MessageLabel.CLASSID,ids[1]));
		list.add(new DataFeild(MessageLabel.FROM,ids[0]));
		MessengerJoinRoomGUIOrder order=new MessengerJoinRoomGUIOrder(list,packager);
		routerData.getGuirouter().executeOrder(order);
		
	}
	
	/* (non-Javadoc)
	 * @see control.clientDataReceiving.DataReceiver#receive(control.clientDataReceiving.MessageForClient)
	 */
	@Override
	public void receive(MessageForClient message) {
		ArrayList<DataFeildList> list = message.getList();
		for (DataFeildList element : list) {
				ListDataElementMessage assignmentFound = new ListDataElementMessage(element);
				messages.addEntry(assignmentFound);
		}
		
	}
	
	
	/**
	 * The listener interface for receiving returnAction events.
	 * The class that is interested in processing a returnAction
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addReturnActionListener<code> method. When
	 * the returnAction event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see ReturnActionEvent
	 */
	class ReturnActionListener implements ActionListener, panelNames {
		
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
//			String[] courseData = {ids};
			sendClose();
			ChangePanelOrder order = new ChangePanelOrder(ASSIGNMENTS, ids);
			routerData.getGuirouter().executeOrder(order);
		}
		
		/**
		 * Send close.
		 */
		private void sendClose() {
			DataFeildList list = new DataFeildList();
			list.add(new DataFeild(MessageLabel.CLASSID,ids[1]));
			list.add(new DataFeild(MessageLabel.FROM,ids[0]));
			MessengerExitRoomGUIOrder order=new MessengerExitRoomGUIOrder(list,packager);
			routerData.getGuirouter().executeOrder(order);
		}

	}
	

}
