package model.serverStuff.serverCommunication;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import control.clientDataReceiving.Router;
import model.labels.MessageLabel;
import orders.SystemOrder;
import orders.guiOrder.DistributeOrder;
import view.ErrorDisplayer;

/**
 * class for receiving stuff from the other system, ie the client receiving
 * stuff from the server and the server receiving stuff from the client.
 * after it receives inputs, it will send them to its router to be executed
 * REQUIRES the stuff being received to be of type ServerOrder
 * 
 * @author brere
 */
public class SocketReceiver implements Runnable {
	
	/** The server inputs. */
	private ObjectInputStream serverInputs;
	
	/** The router. */
	private Router router;
	/** list of distributeOrders which it will give to returnOrder when it returns to the gui.  
	 * it remains empty on the server side, but is included for potential future use */
	private ArrayList<DistributeOrder> dlist;
	/**
	 * error displayer, for displaying errors
	 */
	private ErrorDisplayer errorDisplay;

	/**
	 * Instantiates a new server receiver.
	 *
	 * @param instream the inputstream for receiving inputs
	 * @param router the router which it will send inputs to
	 * @param emergancy 
	 */
	public SocketReceiver(InputStream instream, Router router, ErrorDisplayer emergancy) {
		try {
			errorDisplay=emergancy;
			this.router = router;
			serverInputs = new ObjectInputStream(instream);
			this.dlist=new ArrayList<DistributeOrder>();
		} catch (IOException e) {
			emergancy.outputErrorMessage("error with connection to server in receiver instantiation, exiting");
		}
	}

	/**
	 * "runs" the serverReceiver, by making it continously receive inputs, and sending those inputs
	 * to its router
	 */
	@Override
	public void run() {

		try {

			while (true) {
				SystemOrder order = (SystemOrder) serverInputs.readObject();
				
				if(order instanceof ReturnOrder) {
					DistributeOrder distribute=getDistributeOrderlistBasedOnId(((ReturnOrder) order).getDistributeID());
					((ReturnOrder) order).setDistributeOrder(distribute);
					//if the return order wants the distributeOrder deleted, delete the distributeOrder.
					if(((ReturnOrder) order).getDistributeDelete()) {
							dlist.remove(distribute);
						
					}
					
				}
				router.executeOrder(order);
			}
			// temporatily here as implementation would heavily depend on gui
		} catch (ClassNotFoundException e) {

			errorDisplay.outputErrorMessage("error, class not found, exiting");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			errorDisplay.outputErrorMessage("error, with communicating to server while receiving, exiting");
		}catch(IllegalArgumentException e) {
			//when we mess up one of my order's arguments somehow, display the message to the user.
			errorDisplay.outputErrorMessage(e.getMessage());
		}
	}
	/** adds a distributeorder to the list*/
	protected void addDistributeOrder(DistributeOrder order) {
		dlist.add(order);
	}
	/** gets a distributeOrderBasedOnID*/
	
	private DistributeOrder getDistributeOrderlistBasedOnId( int id) {
			
		for(int i=0;i<dlist.size();i++) {
			if(id==dlist.get(i).getId()) {
				return dlist.get(i);
			}
		}
		
		throw new IllegalArgumentException("error, we somehow didnt find a serverMessages distributeOrder in the list of distributeOrders");

	}
	
	/**
	 * Receives an order from somewhere, and immediately sends it to the router to be executed
	 *
	 * @param order the order
	 */
	public void receiveErrorMessage(SystemOrder order) {
		if(order instanceof ReturnOrder) {
			((ReturnOrder) order).setDistributeOrder(getDistributeOrderlistBasedOnId(((ReturnOrder) order).getDistributeID()));
		}
		router.executeOrder(order);
	}
	
	/**
	 * Closes the inputstream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void close() throws IOException {
		serverInputs.close();
	}
	

}
