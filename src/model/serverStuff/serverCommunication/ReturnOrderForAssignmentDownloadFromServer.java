package model.serverStuff.serverCommunication;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import control.clientDataReceiving.MessageForClient;
import model.labels.AssignmentLabel;
import model.labels.SubmissionLabel;

public class ReturnOrderForAssignmentDownloadFromServer extends ReturnOrder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8092721830376067121L;
	byte[] byteArr;

	public ReturnOrderForAssignmentDownloadFromServer(MessageForClient message, int distributeID, byte[] byteArr) {
		super(message, distributeID);
		this.byteArr = byteArr; 
	}

	// public void setContent(byte[] content) {
	// byteArr = content;
	//
	// }

	@Override
	public void execute() {
		// File newFile = new File(STORAGE_PATH + FILE_NAME + FILE_EXTENSION);
		String myTextPathFromMessage = super.getMessage().getList().get(0).get(AssignmentLabel.PATH).getData();
		Path myPath = Paths.get(myTextPathFromMessage);
		// File newFile = new File(System.getProperty("user.home") + "/Documents/" +
		// myPath.getFileName());
		File newFile = new File(myTextPathFromMessage);
		try {
			if (!newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(byteArr);
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.getMessage().getList().get(0).get(AssignmentLabel.PATH)
				.setData(System.getProperty("user.home") + myPath.getFileName());
		// super.execute();
		super.execute();
	}

}
