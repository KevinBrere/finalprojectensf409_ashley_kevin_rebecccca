package model.serverStuff.serverCommunication;

/**
 * The Interface ServerDefaultValues, which represents a couple of constants used for instantiating 
 * the server senders/receivers.
 */
public interface ServerDefaultValues {
	
	/** The Constant CLIENTNAME. */
	public static final String CLIENTNAME = "localhost";
	
	/** The Constant ID. */
	public static final int ID = 8082;
}
