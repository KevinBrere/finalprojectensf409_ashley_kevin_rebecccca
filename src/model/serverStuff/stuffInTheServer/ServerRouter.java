package model.serverStuff.stuffInTheServer;

import ServerOrder.ServerOrder;
import control.clientDataReceiving.Router;
import orders.ErrorLocation;
import orders.SystemOrder;

/**
 * The Class ServerRouter which executes server orders.  the orders executed are required to be server orders.  
 */
class ServerRouter extends Router {
	
	/** The box. */
	ServerToolBox box;

	/**
	 * Instantiates a new server router.
	 *
	 * @param box the box
	 */
	public ServerRouter(ServerToolBox box) {
		super();
		this.box = box;
	}

	/**
	 * Sets the box.
	 *
	 * @param box the new box
	 */
	public void setBox(ServerToolBox box) {
		this.box = box;
	}

	/**
	 * executes a serverOrder.  this includes redying it for execution
	 * REQUIRES the order parameter to be a SystemOrder, otherwise it will set its error, and will execute it
	 */
	public void executeOrder(SystemOrder order) {
		try {
			ServerOrder servOrder = (ServerOrder) order;

			servOrder.setBox(box);
			super.executeOrder(servOrder);
		} catch (ClassCastException e) {
			// some weird stuff might happen because of this. The burden is on the execute
			// to catch errors however
			order.addError("error, a non server order got sent to the server for some reason",ErrorLocation.SERVER);
			super.executeOrder(order);
		}

	}

}
