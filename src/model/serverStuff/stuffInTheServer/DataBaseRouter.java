package model.serverStuff.stuffInTheServer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.DataOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class DataBaseRouter, which executes database functions.
 */
public class DataBaseRouter {

	/** The statement. */

	private Connection jdbc_connection;

	/** The Constants nessicary for connecting to the database */
	private static final String connectionInfo = "jdbc:mysql://localhost:3306/" + "FinalProjectDatabase"
			+ "?useSSL=false", login = "FinalProjectDataTable", password = "ensf";

	/**
	 * Instantiates a new data base router.
	 *
	 * @param connectionInfo the connection info
	 * @param username the username
	 * @param password the password
	 */
	public DataBaseRouter(String connectionInfo, String username, String password) {
		try {
			// If this throws an error, make sure you have added the mySQL connector JAR to
			// the project
			Class.forName("com.mysql.jdbc.Driver");

			// If this fails make sure your connectionInfo and login/password are correct
			jdbc_connection = DriverManager.getConnection(connectionInfo, username, password);
			System.out.println("Connected to: " + connectionInfo + "\n");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Instantiates a new data base router.
	 */
	public DataBaseRouter() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());

			Class.forName("com.mysql.jdbc.Driver");

			jdbc_connection = DriverManager.getConnection(connectionInfo, login, password);

			System.out.println("Connected to: " + connectionInfo + "\n");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * executes a databaseOrder, this includes setting up the order to be ready to
	 * be exectued.
	 *
	 * @param order            the order which will be executed
	 * @return the array list of DataFeilds which hold the data which the client
	 *         needs from the databaseOrder
	 */
	public ArrayList<DataFeildList> executeOrder(DataOrder order) {

		order.setConnection(jdbc_connection);
		return order.execute();
	}

}
