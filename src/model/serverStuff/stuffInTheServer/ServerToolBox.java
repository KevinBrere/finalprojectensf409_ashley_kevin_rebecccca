package model.serverStuff.stuffInTheServer;

import messenger.MessageRoomOrganizer;
import model.serverStuff.serverCommunication.SocketSender;

/**
 * The Class ServerToolBox, which represents the tools ServerOrders use to execute themselves. ie the databaseRouter
 * it uses to save things in the database
 */
public class ServerToolBox {
	
	/** The db router. the serverOrders use to execute database orders*/
	private DataBaseRouter dbRouter;
	/**the MessageRoomOrganizer, which is used for chat room stuff */
	private MessageRoomOrganizer organizer;
	/** The packer. the serverOrders use to send data to the client*/
	private SocketSender sender;
	
	/**
	 * Instantiates a new server tool box.
	 * @param sender the sender
	 */
	protected ServerToolBox(SocketSender sender,MessageRoomOrganizer organizer) {
		dbRouter=new DataBaseRouter();
		this.sender=sender;
		this.organizer=organizer;
	}
	
	public MessageRoomOrganizer getMessager() {
		return organizer;
	}

	/**
	 * Gets the router.
	 *
	 * @return the router
	 */
	public DataBaseRouter getRouter() {
		return dbRouter;
	}
	
	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	public SocketSender getSender() {
		return sender;
	}
	
}
