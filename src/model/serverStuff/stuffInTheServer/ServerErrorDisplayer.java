package model.serverStuff.stuffInTheServer;

import view.ErrorDisplayer;

/**
 * The Class ServerErrorDisplayer, which just prints the errors. 
 */
public class ServerErrorDisplayer extends ErrorDisplayer {
	
	/* (non-Javadoc)
	 * @see view.ErrorDisplayer#outputErrorMessage(java.lang.String)
	 */
	@Override
	public void outputErrorMessage(String s) {
		System.out.println(s);
	}

}
