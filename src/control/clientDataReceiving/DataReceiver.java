package control.clientDataReceiving;


/**
 * The Interface DataReceiver, which represents anything that needs receive messages client side.
 */
public interface DataReceiver  {
	
	/**
	 * Receive receives a message for client, 
	 *
	 * @param list the list which sill be received
	 */
	public abstract void receive(MessageForClient list);
}
