package control.dataSending;

import model.datafeildStuff.DataFeildList;

/**
 * The Interface DataSender, which is for anything that needs to send data to the server for use in a data
 * order
 */
public interface DataSender {
	
	/**
	 * Sends the datafeildList to the server, for use in a DataBaseOrder
	 * @return the data feild list for use in a DataBaseOrder
	 */
	public abstract DataFeildList send();

}
