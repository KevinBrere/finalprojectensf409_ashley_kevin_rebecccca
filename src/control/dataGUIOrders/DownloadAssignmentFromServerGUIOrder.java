package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.GUIRequestForReceivingAssignmentFromServer;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import orders.databaseOrders.SearchOrder;
/** a order which is meant to be used by the gui, when executed, it downloads a assignment from the server.
*/
public class DownloadAssignmentFromServerGUIOrder extends DataGUIOrder{

	/** The order. */
	private SearchOrder order;
	
	public DownloadAssignmentFromServerGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new SearchOrder();
	}

	/**
	 * when an action is performed, it sends an order to download something from the server
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof AssignmentLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a assignment label", list));
			return;
		}
		order.setData(data);
		super.sendDataBaseServerOrder(new GUIRequestForReceivingAssignmentFromServer(order));
	}

}
