package control.dataGUIOrders;


import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.InsertOrder;
/**
 * the class when executed gui side, it will ask the server to insert data into the table
 */
public class InsertGUISideOrder extends DataGUIOrder{
	
	
	/** The order. */
	private InsertOrder order;
	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public InsertGUISideOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new InsertOrder();
	}

	/**
	 * when called, it asks the server to insert data into the table
	 */
	

	@Override
	public void execute() {
		
		DataFeildList data = list;
		if (!data.isFull()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all data feilds while inserting", list));
			return;
		}
		
		order.setData(data);
		super.sendToPackager(order);
		
	}

}
