package control.dataGUIOrders.messengerStuff;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DataGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import orders.messengerOrders.LeaveChatRoomOrder;
/**
 * this is an order, activated gui side. 
 *  when executed, it will exit the user from the chat room, specified by the DatafeildList
 * @author brere
 */
public class MessengerExitRoomGUIOrder extends DataGUIOrder{


	public MessengerExitRoomGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
	}

	/**
	 * when executed, it will exit the user from the chat room, specified by the DatafeildLis
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof MessageLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a Message label", list));
			return;
		}
		if(data.size()!=2||data.get(MessageLabel.CLASSID)==null||data.get(MessageLabel.FROM)==null) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all relevent data feilds to leave chatroom", list));
			return;
		}
		super.sendDataBaseServerOrder(new LeaveChatRoomOrder(data) );
	}

}

