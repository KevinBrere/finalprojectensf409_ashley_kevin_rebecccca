package control.dataGUIOrders.messengerStuff;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataGUIOrders.DataGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.MessageLabel;
import orders.messengerOrders.JoinChatRoomOrder;
/**
 * this is an order meant to be activated gui side.  when activated, it will join the user to a chatroom
 * @author brere
 */
public class MessengerJoinRoomGUIOrder extends DataGUIOrder{

	
	public MessengerJoinRoomGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
	}

	/**
	 * when executed, it joins the user to the chat room. 
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!(data.get(0).getLabel() instanceof MessageLabel)) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input a Message label", list));
			return;
		}
		if(data.size()!=2||data.get(MessageLabel.CLASSID)==null||data.get(MessageLabel.FROM)==null) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all relevent data feilds to join room", list));
			return;
		}
		super.sendDataBaseServerOrder(new JoinChatRoomOrder(data) );
	}

}

