package control.dataGUIOrders;


import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.DeleteOrder;
/**
 * this is an order meant for the gui. when the gui executes the order, it will delete data from the database
 * @author brere
 */
public class DeleteGUIOrder extends DataGUIOrder{
	/** The order. */
	private DeleteOrder order;
	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public DeleteGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new DeleteOrder();
	}

	/**
	 *when the gui executes the order, it will delete data from the database
	 */
	

	@Override
	public void execute() {
		
		DataFeildList data = list;
		
		order.setData(data);
		super.sendToPackager(order);
		
	}
}
