package control.dataGUIOrders;


import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
//import orders.databaseOrders.InsertOrder;
import orders.databaseOrders.SearchOrder;

public class SearchGUIOrder extends DataGUIOrder{
	/** The order. */
	private SearchOrder order;
	/**
	 * Instantiates a new search order.
	 *
	 * @param list the list which holds the data to search
	 * @param packager the packager
	 */
	public SearchGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new SearchOrder();
	}

	/**
	 * when an action is performed, it retrieves a dataFeildList from itself, then 
	 * sends it to the packager.
	 */
	

	@Override
	public void execute() {
		
		DataFeildList data = list;
		
		
		order.setData(data);
		super.sendToPackager(order);
		
	}
}
