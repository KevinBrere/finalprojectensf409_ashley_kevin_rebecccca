package control.dataGUIOrders;

import java.util.ArrayList;

import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import rebsDatabaseCreation.rebsDatabaseMaker;
/**
 * an order, meant for us programers personal use.  it is for creating the tables automatically
 * when executed gui side, it tries to create all of the tables.  
 * @author brere
 */
public class CreateTableGUIOrder extends DataGUIOrder{
	/** The order. */
	private rebsDatabaseMaker order;
	
	public CreateTableGUIOrder(DataFeildList list, ClientPackager packager) {
		
		super(list, packager);
		order = new rebsDatabaseMaker();
	}

	/**
	 * when executed, it asks the server to create a table
	 */
	@Override
	public void execute() {
		
		DataFeildList data = list;
		if (!data.isEmpty()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input no data feilds while creating a table", list));
			return;
		}
		
		order.setData(data);
		super.sendToPackager(order);
		
	}
}
