package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.AssignmentFromGUIToServerOrder;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import model.labels.AssignmentLabel;
import orders.databaseOrders.InsertOrder;

public class SendAssignmentToServerGUIOrder extends DataGUIOrder{

	/** The order. */
	private InsertOrder order;
	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public SendAssignmentToServerGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new InsertOrder();
	}

	/**
	 * when an action is performed, it retrieves a dataFeildList from the superclass, then 
	 * sends it to the packager.
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!data.isReadyForInsertion()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all data feilds while inserting", list));
			return;
		}
		if(data.get(0).getLabel() instanceof AssignmentLabel) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input an assignment to send", list));
		}
		order.setData(data);
		
		super.sendDataBaseServerOrder(new AssignmentFromGUIToServerOrder(order));
		
	}

}