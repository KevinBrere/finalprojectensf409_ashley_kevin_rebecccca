package control.dataGUIOrders;

import java.util.ArrayList;

import ServerOrder.FileFromGUIToServerOrder;
import control.clientDataReceiving.MessageForClient;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeildList;
import orders.databaseOrders.InsertOrder;

public class SendFileToServerGUIOrder extends DataGUIOrder{

	/** The order. */
	private InsertOrder order;
	/**
	 * Instantiates a new insert listener.
	 *
	 * @param send the send
	 * @param packager the packager
	 */
	public SendFileToServerGUIOrder(DataFeildList list, ClientPackager packager) {
		super(list, packager);
		order = new InsertOrder();
	}

	/**
	 * when an action is performed, it retrieves a dataFeildList from the superclass, then 
	 * sends it to the packager.
	 */
	

	@Override
	public void execute() {
		DataFeildList data = list;
		if (!data.isReadyForInsertion()) {
			ArrayList<DataFeildList> list = new ArrayList<DataFeildList>();
			list.add(data);
			super.sendErrorMessage(new MessageForClient("error, you must input all data feilds while inserting", list));
			return;
		}
		order.setData(data);
		
		super.sendDataBaseServerOrder(new FileFromGUIToServerOrder(order));
		
	}

}