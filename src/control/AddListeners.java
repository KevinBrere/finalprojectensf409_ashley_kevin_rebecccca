package control;

import java.util.ArrayList;

import ServerOrder.GUIRequestForReceivingFileFromServer;
import control.clientDataReceiving.DataReceiver;
import control.clientDataReceiving.GUIRouter;
import control.clientDataReceiving.MessageForClient;
import control.clientDataReceiving.Router;
import control.dataGUIOrders.CreateTableGUIOrder;
import control.dataGUIOrders.DeleteGUIOrder;
import control.dataGUIOrders.DownloadFileFromServerGUIOrder;
import control.dataGUIOrders.EmailRequestFromProfGUItoServerGUIOrder;
import control.dataGUIOrders.EmailRequestFromStudentGUIOrder;
import control.dataGUIOrders.InsertGUIOrder;
import control.dataGUIOrders.ModifyGUIOrder;
import control.dataGUIOrders.SearchGUIOrder;
import control.dataGUIOrders.SendFileToServerGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerExitRoomGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerJoinRoomGUIOrder;
import control.dataGUIOrders.messengerStuff.MessengerSendGUIOrder;
import control.dataSending.ClientPackager;
import model.datafeildStuff.DataFeild;
import model.datafeildStuff.DataFeildList;
import model.datafeildStuff.UserType;
import model.labels.CourseLabel;
import model.labels.MessageLabel;
import model.labels.StudentEnrollmentLabel;
import model.labels.SubmissionLabel;
import model.labels.UserLabel;
import model.serverStuff.serverCommunication.ServerConstructor;
import view.ErrorDisplayer;
import view.View;

public class AddListeners implements DataReceiver {

	private View view;
	private GUIRouter guirouter;
	private Router router;
	private ServerConstructor constructor;
	private Thread reciveFromServer;
	private ErrorDisplayer errorDisplay;

	public AddListeners() {
		errorDisplay = new ErrorDisplayer();
		view = new View();
		guirouter = new GUIRouter(view);
		constructor = new ServerConstructor(guirouter, new ErrorDisplayer());

		router = new Router();
		view.setup(guirouter, constructor.getSender());

		setupServerReceiver();

		tryToSetUpDataBase();
		testInsert();
		System.out.println("after test insert");
		// testMessenger();
	}

	private void setupServerReceiver() {
		reciveFromServer = new Thread(constructor.getReceiver());
		reciveFromServer.start();

	}

	private void tryToSetUpDataBase() {
		ClientPackager pack = new ClientPackager(constructor.getSender());
		pack.addReceiver(this);
		CreateTableGUIOrder order = new CreateTableGUIOrder(new DataFeildList(), pack);
		guirouter.executeOrder(order);
	}

	private void testInsert() {

		ClientPackager pack = new ClientPackager(constructor.getSender());
		pack.addReceiver(this);
		DataFeildList list = new DataFeildList();
		//
		// list.add(new DataFeild(SubmissionLabel.ASSIGNMENT_ID,"4"));
		// list.add(new DataFeild(SubmissionLabel.COMMENTS,"first retry"));
		// list.add(new DataFeild(SubmissionLabel.EXTENSION,".PNG"));
		// String addingPath = "C:\\\\Users\\\\arebe\\\\Documents\\\\ashley.pdf";
		// list.add(new DataFeild(SubmissionLabel.PATH,addingPath));
		// list.add(new DataFeild(SubmissionLabel.STUDENT_ID,"3333"));
		// list.add(new DataFeild(SubmissionLabel.SUBMISSION_GRADE,"000"));
		// list.add(new DataFeild(SubmissionLabel.TIMESTAMP,"TIMESTAMP"));
		// list.add(new DataFeild(SubmissionLabel.TITLE,"ashley.pdf"));

		list.add(new DataFeild(CourseLabel.ID, "1"));
		// list.add(new DataFeild(StudentEnrollmentLabel.COURSE_ID, "8"));
		//
		// list.add(new DataFeild(UserLabel.FIRSTNAME,"retry 2"));
		// list.add(new DataFeild(UserLabel.LASTNAME,"brereton"));
		// list.add(new DataFeild(UserLabel.EMAIL,"reow@gmail.com"));
		// list.add(new DataFeild(UserLabel.PASSWORD,"mewo"));
		// list.add(new DataFeild(UserLabel.TYPE,"P"));

		// DeleteGUIOrder order=new DeleteGUIOrder(list,pack);
		// InsertGUIOrder order=new InsertGUIOrder (list,pack);
		// SearchGUIOrder order=new SearchGUIOrder (list,pack);
		// ModifyGUIOrder order=new ModifyGUIOrder(list,pack);

		// SendFileToServerGUIOrder order=new SendFileToServerGUIOrder(list,pack);

		// DownloadFileFromServerGUIOrder order =new
		// DownloadFileFromServerGUIOrder(list, pack);
		EmailRequestFromStudentGUIOrder order = new EmailRequestFromStudentGUIOrder(list, pack,
				"TITLE: HOPE THIS WORKS, from student", "message:REALLY HOPeING from student");
		// EmailRequestFromProfGUItoServerGUIOrder order = new
		// EmailRequestFromProfGUItoServerGUIOrder(list, pack,
		// "TITLE: HOPE THIS WORKS 2", "message:REALLY HOPeING 2");
		System.out.println("insert be executed");
		guirouter.executeOrder(order);
	}

	private void testMessenger() {
		ClientPackager pack = new ClientPackager(constructor.getSender());
		pack.addReceiver(this);
		DataFeildList list = new DataFeildList();

		list.add(new DataFeild(MessageLabel.CLASSID, "5"));
		list.add(new DataFeild(MessageLabel.FROM, "guy2"));

		MessengerJoinRoomGUIOrder order = new MessengerJoinRoomGUIOrder(list, pack);
		guirouter.executeOrder(order);

		list = new DataFeildList();

		list.add(new DataFeild(MessageLabel.FROM, "guy2"));
		list.add(new DataFeild(MessageLabel.CLASSID, "5"));
		list.add(new DataFeild(MessageLabel.MESSAGE, "anyone here?"));

		MessengerSendGUIOrder order2 = new MessengerSendGUIOrder(list, pack);
		guirouter.executeOrder(order2);

		list = new DataFeildList();
		list.add(new DataFeild(MessageLabel.CLASSID, "5"));
		list.add(new DataFeild(MessageLabel.FROM, "guy2"));

		MessengerExitRoomGUIOrder ord3r = new MessengerExitRoomGUIOrder(list, pack);
		guirouter.executeOrder(ord3r);

	}

	public View getView() {
		return view;
	}

	@Override
	public void receive(MessageForClient list) {

		System.out.println(list.getMessage());
		System.out.println(list.getList());
		// ArrayList<DataFeildList> dataFeilds=list.getList();
		// ArrayList<String> emails=new ArrayList<String>();
		// for(int i=0;i<dataFeilds.size();i++) {
		// emails.add(dataFeilds.get(i).get(UserLabel.EMAIL).getData());
		// }
		// emailthingy thing=new emailthingy(yourMessage,emails);
		// thing.sendEmail();
	}

	//
	// public AddListeners() {
	// }
	//
	//
	//
	// private void addLoginActionListeners() {
	// Login login = (Login)view.getCurrentGUI();
	// login.addEnterActionListener(new LoginEnterListener(view));
	// }
	//
	// private void addProfessorActionListeners() {
	//
	// }
	//
	// private void addStudentActionListeners() {
	//
	// }

}
